        �       	<���������[���6���tSy{	F�)�            (�/�`m f�[&�: �A_n����o�l>�
Қ����%yn���JO N U �k�ft���g��NT[}�gt6|��B��镱��s�?���&�����"ˌ��k��Z�����
�z}�� �g�I9�`�pęHG��&s�G����+�%�����f�6�U�c�����iW��S���O�m¿_����[�B��q9V��#c̒K%�H�T�.Iz�"j�0PS�w����?�O��Z_�NB2�)jYT�NM�&�`MT%���"��p+�Z��(��A�%S��_kɋ�q��(��/��:��4�]A��t���Q�p�2���d�V������w�޵��(�jw��U����r�,�w��Ƶ���;K$ &�v�\���xx 1E���`�~�kJbvPkW��� 7�����y���E�W����L�̡�)�������>ޫt[bE��    �     R  ;     	?     ����Q��s&�C��ƀqѾ�~�/              h  h   #include "mozilla/Maybe.h"
  �  �     Maybe<bool> mSupportsDuplex;
    )     *  Y    	B4   �����2�{�|�Ӯ�l%�@xKZ                     Maybe<bool> mSupportsColor;
    S       ;    	B6   ����!�=��o�2*'E<,��]
�b�                :        _     *  Y    	C-   ����o�� bG0>$��d�Vx:P�                     Maybe<bool> mSupportsColor;
    �    @  �    	C�   �������r��J:_��g����            (�/�`� �	 ֑>3`A ����r��g������u�ܣvC���ޑ��<�	%��.���;zؔx/ 2 1 �Ӈ����uT�}�� � �����r�'t��p�|e�AW@�B�1�S��fl3�ŗ�懨�UFt=7	+�e��R0N�ų��R�wL���S������o���ĚƦ�J��(rvİl�/Ə*�ʈD,Bzf@����[�
�š��Tu��#����3]s���%���rO4�Ě�xF�r����N Z;����sǻ��Mv���Wc��E��J�	|)U/��LR��pX~n��P{    �     P  m    	Dk   ����� ��5�鄠I7Q�� ~              �  #      m  m   8  nsTArray<mozilla::PaperInfo> PaperList() const final;
         I  �    	Dl   �����N���`�O�PW�hL�8              Y  Y   =  MarginDouble GetMarginsForPaper(uint64_t aId) const final;
    b     4  �    	Jt   ������@I��g��a��
k��Ǐ              !  !   (  bool SupportsCollation() const final;
    �     F      	MG   ������� � í��E��w���              �  �   :  PrintSettingsInitializer DefaultSettings() const final;
    �       �   	 	Me   	��������O0hF?�g�K�וx�              �     
    �     F     
 	Mj   
����O.�B�?��ԊY$�����              �  �   :  PrintSettingsInitializer DefaultSettings() const final;
    /     5  4    	X�   �����ŝwS�_s�w��[���U�              Z  Z   )  bool SupportsMonochrome() const final;
    d     K  6    	[)   ������]3���'�>��N��ݹ�A�              �      ?  MarginDouble GetMarginsForPaper(short aPaperId) const final;
    �     C  m    	]_   ����E:�z'%�5�]�%�\@��9              �  �   7  NS_IMETHOD GetSystemName(nsAString& aName) override;
    �     N  p    	^�   ����Ϧ�p�Q��&����h���                Y   B  MarginDouble GetMarginsForPaper(nsString aPaperId) const final;
    @     �      	b1   �����L9pG?���{	�	�,Ћ            (�/� �� � 0�\yR���OZz�?�d��:��)F�)����o�)��]�?��B�c0�- mŇ��>y�-��j��@�9�"TjT�jKv�͒��Q������b �~nv��{���^�J-�
 Yt�ż��ar�s��,E�Q�E��    �     �  �    	o�   ������M{�6��ky��Ӛ֥��            (�/�` � bH!�93̃��ٟ�_"�١�k��7������!�b��X~��3��T$˸�[B�J��M�`�d��59NR�����I�4ک�aY���q�ְt��Z��Z�L%J���俲�� |$@���!��_�4�k@    ~     �  �    	|-   ����r����DT
��TB3����            (�/� � b�`m��q�����B�&M���SU�pWk�*�9�BmݨAV���@�Tq	Jσk��"
2m����@W�u[��� ���F����ῃ��+˿X�	 Wm�O0�v`ʮ�s�v�Z���    	    +  W    	��   ����7��-��}=Hs�&Y=at���            (�/�`� 	 �8$pi���L�D�?��$Hn�S�e�,��(K�H�G]I- ) - N�5�~-�E�^��<u[��Y��e^Ӓ�4�r^Sk�h��� �p8���vX^�B�C��#�	����!�{�.�}Y:�ׇh�/����>7W:B�MF_���k�{��AӖ�:i��
�����r�{�6!��D��d�
˓$�V&�g�c#׳Gw�#k�ǡ���0���wJbhw��F >C��Q��o8exN����p����T�ഢ�\�}Q�ya������1�n�D[���p̓�    	4     �  =    
8�   ����r��� _���Y����-l            (�/� �� 2�"`Q�0kO&O�'�O�X��$)��@��,@�k9�!�x�i
�����T�d.gz�e�ȔCHWþ���8�6,J���}3�Z쥝�I<����A9,Tt�8D�D{8�j��@f
 @ ��X�ّ��H۰���d��8b�\    	�     [  M    
jr   �����y��R��7�R.YH����              �     O  mutable mozilla::Mutex mDriverMutex MOZ_UNANNOTATED{"nsPrinterWin::Driver"};
    
.     K  =    
js   ������o��=���6�P�3�(�t�              �  .   ?  mutable mozilla::Mutex mDriverMutex{"nsPrinterWin::Driver"};
    
y     [  M    
j�   ����{-��_�������"�J�H              �     O  mutable mozilla::Mutex mDriverMutex MOZ_UNANNOTATED{"nsPrinterWin::Driver"};
    
�     K  =    
j�   ����&Β���yn�a�9<�W�ZMݔ              �  .   ?  mutable mozilla::Mutex mDriverMutex{"nsPrinterWin::Driver"};
         [  M    
j�   ����X��gj� �uv�z!����              �     O  mutable mozilla::Mutex mDriverMutex MOZ_UNANNOTATED{"nsPrinterWin::Driver"};
