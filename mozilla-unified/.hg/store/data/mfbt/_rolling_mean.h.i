        �  
�     y���������V��ӕW�_+O��4�%7U            (�/�`�	$ �z�'ಪ #�Iԭ��q�Q3���#g����,���7��`���p� � � �6M�;{��C֮�Z��V�kr�Ϛ7����eO~�?u�.Z�_�K	�R�;�Z�p�!�����jwD�?7�������jT���o26|_\�Z���OΨK&h� ��;�+N,�ZM�[4��dVړ�� ߂�J֏�1��o)(Xxe_�|"�\2�\��0Z;�u�m'n+����{��:��v쭳,��%�ƚ����s[����:�J�x�Rn�jșt�{=1[�,#�i��m-E�Jj9ɤ)wN�	�}�fA��/ �d��� kZ����/��i�Q�[�2^���Rٳ���+HEB�$4&c7� _�N���r�� �@��[�P�7ۤ�P��c�����l���*��a���4�j��܀z�mCӔQ�=��.�wOF���J���W 2
���8�ߺ�9t�f����}1��ѯ����֬!�&�I����[�]x�n����/���߼�n]?��i��,�Q��Y;[��Dd��H�in[��Z�-����_�٩��\�S����sy�30&�_�_����j����Z��$� _��Pv�2�tcJ�\�l,�>0��}����9uri;!7��#r���D���O�r̩	��F�s��DQ�U��F�@ 0�רa)S�0B  0 F�:�(Ǡ"6 J) ���B{jQ_��}CS�$)�^��=XIv����D|����'���䍑&���\��]��q�zkO�&��$���Oa1�tS����L�ceg}	7S$�~�(:F�	T^a�=��:u`��h#����:���Jr'���P7{����,��^Ǜ�_����_��Z� �n��o.�"�i��t�4@R�g���>Z�2������i���omeu-���v��7<�b{$�#
�OC��Uv���� :�nֽ��dn�̃]��{O�+����'�#��� T��mt��2w��	�֙7!& N��J�}�ҦD�z�JG}�U:�O!�i�:C<�G��ߚyT |"���i���y����ר�ytM��� s�M'�ьXB`�ǎ�JX$��W{�ъ�I�	U�%'�������n(���mHMAr���    �       
�     �s    ����<h�d��,J��o��RݨG              �          )        �     8  
�     M   �����:������1P����(�              �     ,    explicit RollingMean(size_t aMaxValues)
    �    9  
X    M   �����4��j��rR�z�5�3��             (�/�`�} F��<@k (E�&���ْ�m������w���-Hl 5bb�v Iz���1�r&q�<�ܶ���q s o �����4�dFS�2c{w0X���~H9��M �"b׺\\����o��U�����zk��S��BY�j��6+�oSq��PnUݼ�Zr&�1�<�AP�y}�b�gS�NY>v��n�t���I�#a�S�90r�k"v1L솘$�NJY򎄙�&��s�=㦓�*�3}��}��J�<
���N	�͈�fD��:8�_s`�t܆�!��@b7���۾�=�a�������Xy�$�8�����|���=�����>?EQ�v͔{�Z���4�X���L%9���N��%o�k}��J`�+�
=M���'颳en~�(
M�g�i��(��`���E
Q���:�v;@v%�BG�38?ۤ�k����kd�]��	��+��UL	����6�H��'>v�+v?P�F��6�ON�~=cw=P�j��q�O�&)��q/�5�C�����1Ӆ�~�!-S ", !(�0 ��3� &!CHT���X��]j��o�躽�D$k��|H�}9,Y�ca@��@���6F�$�U㎭.��3a�WZ�8m�Nk�&��Q��m��F��<G����k�(�椁YWX�	ڞW�R�@�#�=X�}lJ��jRŰ�+,�a�"tX`�| ��/'�ZwY�x�����x�ϱAE���(������8^.8�0#8S{�GGc6�.S°��3F">JB�����&�(�}� �&�)��ͤ�쐥q>����G��
�T�
b*q�         >  
a    ��   ����RPN�`��B�=�˔�@�.e              �  	   2    return T(mTotal / int64_t(mValues.length()));
    Q     D  
i    ��   ��������h�k<��S���Q�              G  w   8/* Calculate the rolling mean of a series of values. */
    �    �  
K    ��   ����Dre��)P�22Մ��-            (�/�`? ��L:@k �?��w�����u(����W���mR�Q���,l��-����t����= ? < �G�d���L��N�+D~Udd���>
Yfl���U�\f-�Pʥ�̩4#s������< ��Ox������U"�.()2*'���C�������9����8&��e�R&��#5#RYA�׊^�g�~�Ԃ�z�'&�N�^�7�O E����o����K��$I���x	�_�hR�M�j!��`(4�	[�`�b�5�53l�Z�� ��-�5bңU-姬�@�t�j���J�� dˈP	���]���-��ތ�@2O6���\σ�ı�?Rh�%U�5�a1�ǌ����v�r���B�(O    
      F  	N    ��   ����C��'ኚJn�ͳ�ɒr�              �  �   :  RollingMean& operator=(RollingMean&& aOther) = default;
    
f     �  
K    �   ������v�,!�1��;�,!Z.�            (�/�`C � ��&*��	3̠9�f�G҄H��&B� ���d3��z@XX)P¸�Q$	ß<g٘+�h��<��^M���m<=x���ܱ���Qx��)Șx�O^�
׭M�m+X��b6�2����I��d��Ŝ�K�d�*�����`�^��ccc :8��(Z�?f6P40�4�R�J�3�A8��z�=qLVD�X���|    B     F  	N    ��   ����WbXy[�Y7���V_Z]���              �  �   :  RollingMean& operator=(RollingMean&& aOther) = default;
    �     ]  	g   	 ��   	�����۲b�"�?@��d޹)B�j&              0  0   #include <type_traits>
  |  �   .  static_assert(!std::is_floating_point_v<T>,
    �       	G   
 ��   
��������+�71�eK�$��ȗF              �  �        �     z  	Y    	�b   ����zC
�>"v߭.��<V�^�^            (�/� �� B!�)��0F��Y&�$��������20&I�#Ɍ�KT�y��S`���ï _��B�Jd�=|
y��:�-Z�ߨ�_$}�5F	�0�h?%�$} ?�8�M�!B�ufSP