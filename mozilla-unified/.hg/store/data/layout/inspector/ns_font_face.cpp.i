        U  X     }���������	�l���c� �P7���[��            (�/�`X]2 :D�-���C@^7��֓i��4~�dxX��&ÙCj��S�Z�)
\t� � � pF�3I���ɂ�L[�n�"�?���'��7�gC�7\F�F�3�g�w�ۘ��ɶ-؂�f��G�
n����%~`����2V����?;�>?t	�k�F�*��3*�륏m�`,M���   2�����e��f�����<P�#^�>6��RvN�ک҇�����Ӓ#���@�sM�\O�f��.��7�Fʦ���[�m�AK�(x��^�,	�фHķ��Sp����%f�Yb�z��\*�� � �U��.)��7w�Y{}��Ei@�,&�*�s�4��y�W�u�Uv�4=t�� E��'�q���_�3���:�4��N�Y�"�ge
d��mS����_u���m�=	y��$.�b�0�gW�=	�wb'�y+���Lue�+����%~���=
��
V�ۤR�U�(������R*���p��������mC��o�ߟ��f�N�;Y��ϻ���]��댰5���Rk��'ո:�c��s OSK�����):2r}������"��Z9n�7���*쪼7a�dCj	�G��"k��1�3����,M��9����FAی�7_I��w@��^;��y�z��6��2K$��Q�֟n�b�8�M�p���Y�s�F-(�z�U�	ϑy���s���l2���_�\؅w���QnT00�
��`��eQ�����Dޟ�ZR�*�7�4@�︊�js ��r��~}S��"��o�R�-���@a��ɦw�$�`�J���RHf    A p$i�T �(�$D9��@$� @ C# � ���O�mzUɷ��G����a6h.ȇ�ǲ�����D���ҍ*a�s,�<�P�>�]	F�0���~!�)`iҾ8�x2�{qL���UpTۂ�؏�� �u6�j��:�R1xяHH�0�
���J0���JL��ҡ/[!�@�_I�����h��/;\��<�d4ܟt��i��v��]v�7SQ�yB�B3�Ie-�ɑ�4{x���U�X�;p��Ln%U����#�bT$�L��MK����EPz�M'�~��� �F1tΉ���!rX�-�������x��H�7k.Ō%��֒/>�W&���b��"���ڡ�O�p���ɛ,Ȣ�;#k���dx2���ZB�c��y �:��ii/{D�N9ѧۡ�+96kI:��UPa͟��;��
*�-�]ڙo���Ybw�S�,´ȕ�JЕ&�I}�C�C�5��2/ee;�B"�uo/����Q�U�"��������v �V���B��?@)� �S�C���L��W���}>~��%�,��Ѹm�N�v�h�t�m�ۜͼf��MfY~�G$2��i�P���8X�-�J��߱vb�?�Da��O�9��F�@!�+W���gn�j��g�}�g��H���܍xC@\���JY"��F���eDQT�1����ĵ�]"�'(��wt��wאj)�J�G�H����bn��L	Ş������O�P߬@P�G��5w&    U     >  X     }�����������+���鳑���$�               ,   ^   2copyrev: 01e607966a1959cc2fdc23169cd1646e042b76cd
    �     F  �    �   �����bρ��wJ����*�c	$                   `      q  �   .NS_IMPL_ISUPPORTS(nsFontFace, nsIDOMFontFace)
    �     $  �    R�   ����(�D�x�)}�e�ܔt�e              !  6   #include "gfxTextRun.h"
    �     �  �    _�   �����^�h)�����9��y�=n^-            (�/�`� % �'*�)�w]�)��P@�l��4�N�D��o��������]h>�av4�R�-:�4���?�K�����K`v+:͐�1�rB�Y
�.��\���y\r���d������R,v#�F�����ٚP�+~!:Z�a04G�-�Q*�ͱ�s2έ>]@�N � V(p] f#������ê!�G��`�r�1@��8��
�~CV��
�e
M(5l4Op�)�    �     u  9    b,   ����!��]0y�#�km��<���              �  �   i    if (formatFlags & gfxUserFontSet::FLAG_FORMAT_WOFF2) {
      AppendToFormat(aFormat, "woff2");
    }
    `    �      b-   �������j8^~mJ�$��+�"r2d            (�/�`j� Q;)@��Ã�䀃S�Z%ۦ'��3e�4ğ�e���23����V���21 . - LA������i�\���Q���@c�E�΃��񲠷� � `,SkЯ�������=;�d���WV>�k%��4�$#!����F�|v��p�hv2Y�Z�+g���-�����O��:�k��7��xzЯ�5V���M佯&Q��¡~��um�'�<�)�½u @,������� �B$I4  ����E	�k�b�X$Ė ���
x:@[��g�A}��z���F���X�tV��?��qI*���'�0h��T��A����)4*�:8>7���83G���Z�17SX��1�s�7��C!    	�     [      =?   ����:=�~��&���XQY%R�            (�/� �� "��և�&�0��~��C��kp��K���h#Z;�փu�>��4!�܏��A+Pəq�S`^�
 �c�1�    
;     Q  #    �E   ������>���G��"t��M2��[            (�/� WE �  �  �   K    const gfxUserFontData* u = mEntry->m.get();
 M1�i	e    
�     q  R    D'   �������{>��˳�b�Z����8              V  �   e      nsresult rv = mFontEntry->mUserFontData->mURI->GetSpec(spec);
      NS_ENSURE_SUCCESS(rv, rv);
    
�     �  ]   	 WO   	����2I�₥`��N���)ՊlTl            (�/�`M 5 �	"#`�0p�M<�d���!���qI�\�6�K�0�L��󸄋"᫼�gĻb	�Oa�%����-�}g�/Fc�����ŏ2>?��!~�޳�dZ�<+؜��(�m�ح��Ʊ ����gs�8����	��ʹ]L�
 �B���z&��f	PT�<%��� P    �     s  �   
 �   
�����U�$�L�l��C$V�n��            (�/� �U r���
 �7�\6���}I��*��̅���)W��?*�{7)���5q�C�r��� 0ǋ��4p�&�lu�����
���l�SQ ��`�Y���! ID, 