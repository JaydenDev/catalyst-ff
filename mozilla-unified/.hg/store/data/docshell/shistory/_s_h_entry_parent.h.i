        �  �     ����������ˁ��Jw���*�RY�K�            (�/�`��D JS�,Ќ���F�r��2��_k���K�y�=��a`6�"W305S� � � �| ��Aw�y\�o�*��QA6e�.�*,�*����.<�� C$s�l��X���ꮴ�>J��Z#56�����åx�֪���iv6�ӖQ�u4�]�iSJR� �B8�8ǻ��H�P�a�A�䅒��|�V���4+�ZSB蟤���䤼��d���N+CC���ܬ���*���Q�D*�3Eɇ[n���	�h=X����/�N��귝���N���O^�=4���_u�������Д]Ih#B_l�F�]�kt�s�`�Z��BG� �ALv4���I��V*kU��9���M��M���D�n���>υNP�4N�?�BP��s�#�er6��o%G�y&��Դ+
踘:�F�>��b�T���N�늫����Y���ݚ���no^�E��L:���`��A�<j�g��]|���}�j�83��@���0&L$�=5vE�u�4KLߒMpۓ��4?�)�M61�i��;{��J�-��t����k2�o��uq'�o��Ԑ2M#)3!qD0C=I�����Q(�&�(�F���{���WC��i9�7-��[}���R~�u��hh�/~'շ8<�U������k�Q��4�&.c�7�&k�tз8�28J�����>ژ}A�E��Ru4��#$�����-ǵ��������5O$Ҫ"�(�/~�y,�4Q�p��u�@���pSlzK���C�K�9Ya��K���Xj�S��n�\�V�Y�2�LG~�	�;�}�: DP�e����硎��L�äDs�D�6,�K��r�sKзLN>7'fT�JE��;Y�[��Ѧ1��%���Ŕ�-���wb����M��w�����f�sz_j��p��2
G���}˔�A�R�*�
ЯL�Z�3��l�!e�   R ��i��@� Ȃ )�D�@  � #t�b��鍫��V?��ʹ�v�%�6B�K��E�Ic�Q}!y�;�1����K��$�(�|��S�X{�16ON��jr��K��`9�D����l'̀D�9�>	��9��nǇ��vkB9�.;��8$T�U?������̀[4�����+�c@��z�ޫ-�U�(��1r�=n��sZ��?Y �3��z��G陾������N��u�	���[|o_h*���� Q��C�3�\�{����8��
�$�t��EJP�Z5�pϥ�˹��u� �E8���hv�����n�l6���J�O9��I��'T�"�Kiy�X4�-�@���ժ�g�:�]jm��W� ��VR�,Ma�hQ�jkw1A[�`��xXQCT']X�Y,T׿(X:$Bkra@�9
����3����j�c !yR=�J��[>�,��<c��4�݋�I���+R��Ӎ��.1P�Ʀਜз�m��0�Bm�8���s�4��&�1�7�%���Yi�:n�gEԺ����^z�u��%6�p�Y"v�泑��(&�����ۈ%��,E��%�3:`�VM��[M*��F�H��*�G�U���;{�n�jX����U�y�ж��f��ggYS!Q�'�� �l�4���)�rbi���*Sfr]���@c~z`K�rS����l���Ĺ�X��{�s�/B?S��_&B�#l���WſfR��߂0�T8z74�v��(\/}?���SA�)!�ėVe���O���׀z�g�Uܤ��1������AlY$���!��,%�Od�ώ��ˊ�,���V����zf����ܸ\y�f��� �k�?^rb�#���À����o� ��x�s3�	Q�&�(+i��hn0F��b�S �ܰ0��5���V�9�R�+��/���(�/�س�P�����k�Y㳰!8�_��\r��`a}C��P5����fv���J��B(Q%������޻L������)'mr�8��!%s}ά�"�v�x Mc4%����OT����9x=������N�ōy��Cx���q���U��,�),�eK �ͮ�j���3�� �tv��돩�yʽ��� 7�y��!�j�y�oL���}cq긖��,Y~+�S?io� �C�I� 0�=�?�Wz���nq����>�����V� ��J�l�"�    �     >  �     ��    �����6n��Ov3�7�<Qv���.              
[  
[   2  LegacySHEntry* GetSHEntry() { return mEntry; }

    �     A  "    ��   �����5�w,2��}�����f��p�              �  �   5  bool RecvClearEntry(const uint64_t& aNewSharedID);
    	     �  �    ��   �����l���P7�Hf�Q��0\�,x            (�/�`� � �M,#`i۠�V�Q�Ldq�^l�_/Q�W�o�0V��q��w�B�3����6�	XnȈ�e��B�?����o�HѦ� �gƵ�<f����"k�~��u�ǲu
�e�v�!$.�dY�����@S-�X���:�=�@3��#���DJJJ	JJ���w�W#���4T�9~ެ�)� �� ;��xj�Z�����U�5Q�b�:A�	/ �U�1��TfF���>�H$t�f!pӍ�,    
     P  �    ��   ����K�,��gl��F7q=�33�              @  @   D  bool RecvCreateLoadInfo(RefPtr<nsDocShellLoadState>* aLoadState);
    
h     y   u    ��   ����c��撦�D��e��h��            (�/� �� ��`o@��K�o��K��>�W�T�@�b@�\�Q��q׶�/�����7nu� 53�X��4l��gF#�e�k#R���� R
 C�H�\�j'c����    
�     e   �    ��   �����?���D�=�~9�#BE              �  �   Y  dom::SHEntrySharedParentState* GetSharedState() const {
    return mShared.get();
  }

    F    n   f    ��   ��������^}k�Ů��52Ļ            (�/�`U% ��G;PQ� ��ׯ�?��~������aw�����IRJB���i%�da˱ET+�-���(��b9 8 6 Mc��F�h1?�q�r�H�OE~X)5�n��g�4&h�P�"c�]&R�qXS�gw��~����uޘuCȹD�vy@���P�Ul��.���/�+� ~�j<����Z\[~�t����dȩ5��Խ�_@��< �5���#Ֆʣ�#�BXb_r��Z+���r3s�׺�<����'���?��h�٘]�b3d�A���W+����W��� �4�}���9� � ִ4@bV\� ����6�%����j��w��y�6���k�g��Q9>8"    �     I   �    �   ����_fc&���%�a�>�e�2�              �  �   =  NS_IMETHODIMP GetBfcacheID(uint64_t* aBFCacheID) override;
    �    {  "p    �   �����}�݂ۚ��?�t�6            (�/�`Q� v�:1p���U���NBAK��zH�t�HjR����Ʈ!�Aږ�X���Ar�n۸s- - - rB&��������j7[�v�jA��"��k���*���jcՀF,T�b�sua�Y	!�Փ(\�v�wߋ���S^^�>���x���������ҭ���2���rH߾�2���&DjD�vY�2a5���I/z���Fٗ[��Z�P�`��TUB@5@�hT'2�:R��S�݅x�4 C���B@�;q0��
I�wC*)�]��x%��A[�!B�<��0|P@��srQW�`<\{�1���
�X�?��r�$h���W����c�B�*Ի͡�-B���S���3��0     x     �  #;   	 �$   	����Co*��d��++x%�xElnS            (�/� �� �!�	 0���"'�wI���z�	vlg _�vB$�$���ǫNf��/%�D_!Z���C�Ɣ���zƖ��޺M �W����l�E#�G��Ʊ]ȝ�\ � -�:C�h G���e    �       "p   
 �>   
�����0?�QX�6�Y�Aȟ�ë>�              !�  "�        	     �  #;    �Z   ����P)4�A�Gn$��X��KB�>            (�/� �� �!�	 0���"'�wI���z�	vlg _�vB$�$���ǫNf��/%�D_!Z���C�Ɣ���zƖ��޺M �W����l�E#�G��Ʊ]ȝ�\ � -�:C�h G���e    �     *  #C    �   ����kҟ�8���C!G��9����              
�  
�     ~LegacySHEntry() = default;
    �     �  $�    ؃   �����7� �+-C�JCia�c��            (�/�`p } r1+��3�0�3 �7���vgķ$���EJl�-[��|N��Z7p�k?nAl�*�ГFu�k���#:��L����f�I4�Ed���XҾ`��\�Cg�L��eBS�������Z��.�C(��m�>=�$����v追a<B����Q^�T� @C@k�-[A��:��F���ydFCz��}5�4&��I��M]k :	��ꀱ��V-�Ope�I;�x\��}G�� ��`�G    �        $�    		�����������W�>�ys�	����U<�0�            