        �  Z      \����������G�z�`��Ca�������            (�/�`Z�/ �K�+���P�r�)�@�a��$Y����M�E�t�>�Kl2�6�ac!� � � W�~io��=��4�UC�*��d(�8N��к5�����=�k/8���\_t����5}T�����˾q���� �Q�J1u�>��U��~?e���U�+\�s5}�?;jG~W���Aj�.Q���p���y�T�/�$�X$�O�i��Oe�����x��ub7���������q��Q��<ܡ�k9�  �C/��a��E#���I�Jy�D�b��Im�0:P7�.Jmy!~��z~���%�+5�?��20~�TH��t#u��r!��p����#�S�$S�
[.ģ[	!Uc����K����ȡ��Yi���]o��Wt��ɬ|&n����5�Lt�o�}�M�T�C��%N���TM��!��_0A~����\ 
nEXX��|B��$<�G�+5nq�b�Oь�?�K�i����v�:�j�~��Ը�s:��K�*d���6V,�H6ŽA4PG�N(Ռ	�k�/�£q1��	�4�L��d�,H%� �j�����Ux$����Nt飼�Syw��%wk��ms̞���V��ӫ�[�DUtr2�#o.7�pB�����C�a��N������BMss7��5o5�Ǖ �s�Đ Q���^Q���y4�e���T��Z����j�5#��h�|�����:	��L\�dF~���Qo;�6��"�H"}N�����A@��B)d7�u���MU��<p��Ĉ_˛)^�!訛���2��Cf���+�`�/��Ui�uŅ�nk�����h�G�\���T"��$+�LL�De],QERd"��;�>�I��o4����h�M������2
�  @  �  ��@K��c-�r�1C��� �   � �]���� a�
�����F���m�Fd ���:;��n&5��WXI�)��!Ѓ�2���l�Pqҡ7?�l���ƞ��f�H�$+bb����~�jC��La����o�P�������� ���换��E����E�u��X��$ր[T)I�'�� �`�@�_�E:����*���<s}kz�{`c:&�Ӂ[��s��ΕR��8l����R3�'K2�P���jU�0�,n0��	2�1��N(	�@�g�2�%���|����� ��6X�Z��ӡ.���*�vѕc�j���b�#�`�_F��BUd���,�:K�5B]s�ǻ��>���=r���]KF��[��{�&�C�q����o{9� �Ă��l�LxA1��P�|?�80Jl��4t)B��f�����M_�	}�͢/��,iD�[<9$���I�g.�&l8oلiS��x���? �N9VAd��^�4d�+$O9!PD��G27�	̝0�8hl���fMf����
n�ƹ���U��k?}l',��0��osT�=1�1ԧ��x̑�����I�K;�L��]O�~����hû�e���f�j񪄫�P�    �             c5    ����3��d%��d��������h              �  �             K  Z     c�   �������E�K��%O�S������              �  �   ?  // Implementation helpers
  nsIContent* GetParentElement();

    V            e   �����֟_���b� Ȧ�y}?��f              �  �        b            v�   ����	7F��=�3�x�Ǜ���Zh�              	�  	�     // interfaces:
        >  i     v�   �����{
�9g��gNsr��KC�+��            (�/�`t� ��^5Pi� �@"EG%c��F~^��r@
�?S�O�������)/I��U
e�o][ K H  H�4��9�a���0��
��σD�LR�RQ�#U	%�"���"���D�CEI~'BSA@�8C�b��n���i���]�[X�@C!@��ohx�N[rŃ����;���r����l���〸�v����c�{�tXB蓒�E���2�i�p�X���ePb��� e���j1��߶�1��g;�,};7��1��/��s��f{O�8Pf},�����m�{���)�`˽V.�Ou��+��&&J5M�|i�Sz���R�{2��JZynY��5�z�'���	�>Ȭ�T<:ͧ*Ap��h@�r&�E��]E�!�c��Pf�b  #0�">��LMSF	�aj��Z?�c�"6�7�'xڳG�L�n��?�H0m/��>�2 �a1QU�q���-L����'8���L�>c�:`���������[i̭QB��O,�����Y�ag��|
���&�!~����'2�hHL��
�)�����)�[����Om�M    �     g  �     ��   �����۱&Sw{��$���Ɇ            (�/� �� B���	�Y���D��	�(O����\Ry���Mep����ľ�M������bsa���wd�R-����f� Z �i�0͘�    	$     O       ��   �����N�i)��t�P�п��Z+�              �        �  �   7  virtual PRBool IsNodeOfType(PRUint32 aFlags) const;

    	s     p  �     �U   ����G��)Rk20F;�m<I�uw�            (�/� �= 4  �  ;   w    virtual void ElementChanged(* aFrom,* aTo) {
  nsReferenced::aTo);
 Dd�	��j�E8%�e    	�     H       �V   ����"r��)A4z�ڨ��?οE/              �     <  virtual mozilla::dom::Element* GetTargetElementContent();
    
+     N     	  ��   	����WV���'���[��BQ!��k              	{  	�   B  nsSVGAnimationElement(already_AddRefed<nsINodeInfo> aNodeInfo);
    
y     O  [   
  �B   
����OG�"�"�lj���5��              �  �   C  // nsSVGElement overrides
  PRBool IsEventName(nsIAtom* aName);

    
�     n  K     �u   ����#�ͭ�R�H�R9���z            (�/� �- �  �  "   L  virtual const Element& As();
;
  �      .* GetTargetContent();
 Aȼ�fM� �੍}|g�	    6     �       �v   ������H�4�:��)Lmz�ſ���            (�/�`W = �N0,�9��|����#2������M�wUz�u��$,ꏅ$�l�k�D�_eJ)0����Ã�<��&u��F�ٴ>�Leiӌ�A��f��"*ĉ����&r�\.l[O�6����@ Dǀ���7I�j�21���ƴ˽���6���ڣ�[C	�d�)KI���P&�G׊�_"��������[�#� 3A�>?� aTA|�������ϻ���t����    '     w  p     �c   �����{��t9EmE���8��            (�/� �u   �  �   �  virtual PRBool GetTargetAttributeName(PRInt32* aspaceID,
 nsIAtom** aLocal) const;
 �,�B.�    �       )     ��   �����0�@����v<��0ԁ���              e  �        H        �    �      /�   �����\B��b��`��c�Lj�            (�/�`� �G7PA ����grI���jϘ`�۲�'�c�Hw��G,������!1�@�� v}Cv�Ć8 9 8 �a1v��c�L�T�MZ�$��f58A_��v�	U)GM�KK5p��6��9���?��g�?]i�ŕ>���ct���T��Xy{��n2��\H�q`mb���B�#Bس�!���G�%]��ۂ��z+�f[W����l���y����y+�HH���r/������rq�q����:�N����v�t'��ZrWX)c�2�O#�B�^�@~ I�>M�e���x��]:& 0  E�~~�nb���H/>&C���ah��C�'� �����r�{a0�)���LPa�3ކr� B��������� a��b��l(�o}��    A     F  Y    GJ   ������s(�x�,�Sٵ���;�5            (�/� c� �  � #include "DOMSVGTests.h"
  � 2 public ,
 C0�v�    �     j  B    N�   ����
���oi8���C�*�K�^�            (�/� � �  �  � 8 '#include "nsIDOMSVGAnimationElement.h"
  ] !Referenced} SVG I*���A%w�8.e7�r    �     ?  u    Q�   �������x���u�%��[�2R{}              �  �   3  virtual bool PassesConditionalProcessingTests();
    0     D  w    R    ����D�v���h�b���r:��	�            (�/� V� $  �  F   J const nsAttrValue* a, bool aNotify);
 V�_    t     E  u    R   ��������[�~���kKb�L�            (�/� T� T  �  H   H const nsAString* aValue, bool aNotify);
 �    �     D  w    S�   ����EP���~[���0gXhT����C            (�/� V� $  �  F   J const nsAttrValue* a, bool aNotify);
 V�_    �     M  v    c   ����I�Db�ϯ|�����gѕ�a�              	�  	�   A  nsSVGAnimationElement(already_AddRefed<nsNodeInfo> aNodeInfo);
    J     N  w    c#   �����65����,N.�F����              	�  	�   B  nsSVGAnimationElement(already_AddRefed<nsINodeInfo> aNodeInfo);
    �     O  �    q|   �������&��Ҭ���F�p��aa�              �  �   C  // Utility methods for within SVG
  void ActivateByHyperlink();

    �     �  �    r�   ������8�Z'V?�D/,�=            (�/� �m '%�'	���#�$���h���2Q'1T���v���:�?GB�%R�,��[���`t��z�}S�˾���?��+��ƙ취kV���O�Ʈ�7G�8B�!����ȏ���+�Q�]4{~\�����+��g��6��,�s��geG�"*�
" �c����p�=    �     �  �    �^   ����V��c�OW4d �G8f*�            (�/�`o } &+�����$S�dvS��ޣ�}���I63I�DӓP?9�D�)(Vz/
cA�z�_$�D� �+��O���W��.X����=*����eR��/b(�{����-�+�v�.M��ځ�es -�ކF�� ��Z`��ˮ!�E^�.�ƆDf��W0� 3+�c�. X���`�
��Y��J]M�c*��А��%���Y*
    v    �  )    ��   ����Aruڿ���~�R��            (�/�`� ��F5PA �B�:�3 P p�S�O������@�l&����+��^HŚ�dGNlx8 8 8 ��,�c�H�i�M��$���jp�6��)�J�R��|��,�p��6��9�k+�`��H�+��]�@W9?K�Ћ��	-a�e ksS-����@�����L�T�Z2}W���P����mveU)٠��3�-�@��� �NB�~��\���� �/Q,Wy{���V.,�����k�;���%w���U���T+�����4��˚3�8~����t�j' @���M`,tj�P��,)��(ü�?Z�%���׀C{P�@���8��e�����_��a�pd�����)\n����a�B�a#"����        �      ��   �����S=���W�oaL�s��_
O            (�/�`� �G7PA ����grI���jϘ`�۲�'�c�Hw��G,������!1�@�� v}Cv�Ć8 9 8 �a1v��c�L�T�MZ�$��f58A_��v�	U)GM�KK5p��6��9���?��g�?]i�ŕ>���ct���T��Xy{��n2��\H�q`mb���B�#Bس�!���G�%]��ۂ��z+�f[W����l���y����y+�HH���r/������rq�q����:�N����v�t'��ZrWX)c�2�O#�B�^�@~ I�>M�e���x��]:& 0  E�~~�nb���H/>&C���ah��C�'� �����r�{a0�)���LPa�3ކr� B��������� a��b��l(�o}��    �        Y    ��      ,��
<��S4B�k
6���Y                �        B    �b      d�`{o\�˲�a<�����                �     ?  u    ��   ������֋A�<�Iy�s��;���              �  �   3  virtual bool PassesConditionalProcessingTests();
    �        u     �       �[��Ռ-;!	���D                �        w    �5   !   Ȼ\��FwqC��dF�~���                �        w    �   "   �LV�Ƨ1vx���v��WD�                �        �    ��   #   �w�)��)2x�4/��H����                �        �    ��   $   ��ƚ�ј8z ��M�d��                �     )  �    ��   %����q��.��"���f����M#�              �  �     // Element specializations
    �     [  �   & Ѐ   &���� ���{z�ՆZa�L�'�$              
�  
�   C  virtual bool IsEventAttributeName(nsIAtom* aName) MOZ_OVERRIDE;

  B  f    