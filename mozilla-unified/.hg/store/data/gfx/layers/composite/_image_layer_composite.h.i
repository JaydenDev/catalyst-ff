        $       ����������2����?����+L���b�>            (�/�`� 6��'�: ��>�I�(D|JgLL?����i#��Gnl���Js w x �v��{���3Up~�]>�P>�x��d10jV7}5-Z�6_7�}���ߐ#����ot"��Y9C���5f�(��+�M	�u-	��E�I��-iɗ��(���bGbm_
�<�kf.y$�ѥ�Sᵮ߻���p����G��e8+7ә�fU#��	��8_�et<����6ܱNn���a�n��B�1�$�A�'���a�ġs�AP�ug�=�y��y���|x���՘����_ܝr�0��{.4���,�j��D�A��O�I� t6)�|�p1�U�*'|p۩~�9�q[<Y.ei�l 4�C�	�ޮ(_��'A2�m�4N�O�������3횂�H�0W���x�sz=t��$�'m'�=aLq�D��{X�$;("�t��d����e��82Ү*UF7䞔k4�]�C7�	���P���(�C�IҮ����'����e��E����Z�9��T�쟇fmsՐeo���q��Ф1����T��0�2��f� �e��0����@�%}����#(��I�,��� ś��S�Q�I�gs����<����w�A��A9A��wqy�hV�u�F��a�I5#�J}��{ ��j�B9V$��]#�]��7�1L7��	LBYX&yBU��,����B:e�0�	y�MZ�-�ax��B�x&��H�$��N��h�D��:wM��&H��A��ѯ�,6��ȄD���ɅV��HyuW��+
�:J    $     :  )      1    ����D.��ͫ�hQ���8�.п�              a  �   .#include "mozilla/layers/PLayerTransaction.h"
    ^     0       9   ����
��(\��J�D?C	�4�%�zE              a  �   $#include "mozilla/layers/PLayers.h"
    �     :  )     T   ������R�1 �����`�.1�              a  �   .#include "mozilla/layers/PLayerTransaction.h"
    �     �  �     X   ������ǭOl�`�xdx�Ʌah            (�/� �� �"�)	�cUUc[��H*�L�$~����3��h��A��-����S��7���1E��ɧX1�6�W�_0�8�F\�у	I����`QRI.�]7��Y���Anp�/�-� 0ލ�9�
l�+�U�    I     �  )     \   �����%FX
�0؟L��p��            (�/�`3 � "�..pK�������Dw�i*�IQ'�J�n�H���Rf��*�	��Y��@�H;�N;��8�%@D���d�����>�l2a�
����r�U��M%�}V06��+����O�)��g���)�?���`l6�����ͽ�:����«J_���6k,;]�)ȭ(����|%�5� �[�a
 #̀q	�j�b\�˔�1[e    ,     0       b   ����j^`��ysX��_���              a  �   $#include "mozilla/layers/PLayers.h"
    \     :  )    8   ����V1��_ʨ �6��K��'L�              a  �   .#include "mozilla/layers/PLayerTransaction.h"
    �     �  �    >   ����A��H�v���H0��f9;�            (�/� �� �"�)	�cUUc[��H*�L�$~����3��h��A��-����S��7���1E��ɧX1�6�W�_0�8�F\�у	I����`QRI.�]7��Y���Anp�/�-� 0ލ�9�
l�+�U�         �  )    C   ����}P��4����@7�.׌�%��            (�/�`3 � "�..pK�������Dw�i*�IQ'�J�n�H���Rf��*�	��Y��@�H;�N;��8�%@D���d�����>�l2a�
����r�U��M%�}V06��+����O�)��g���)�?���`l6�����ͽ�:����«J_���6k,;]�)ȭ(����|%�5� �[�a
 #̀q	�j�b\�˔�1[e    �     �  �   	 �   	����H���}�1g��5eϣ{            (�/� �� �"�)	�cUUc[��H*�L�$~����3��h��A��-����S��7���1E��ɧX1�6�W�_0�8�F\�у	I����`QRI.�]7��Y���Anp�/�-� 0ލ�9�
l�+�U�    {     m  �   
 �   
����E+g"�I]VT�@ �=����              �  �   a  virtual void ComputeEffectiveTransforms(const gfx3DMatrix& aTransformToSurface) MOZ_OVERRIDE;

    �     3  �    ,�   �������/Ď����7�A�M��'>              ~  �   '  RefPtr<CompositableHost> mImageHost;
        6  �    4i   �����M^z���:�HK}%���            (�/�`�e	 �3-��3�0'p�1`���9���-��wϕ���&*�-�1=M d"�Gԝ|��\�@|>8N6@	U����%������/�wơ�����j�߁�
$T!Uƒ�b<.{��E=�E�.ȿ'�e�rj�$;�"����Į�h�V����H��р�s$@ 6���4#t��7W6bZϪ5MS4��ĝ=�%��1��'�@Ģ����% P���G�`�cc"ሁ��I�v�y0��/IଫI��k�,GӴW�\ �� N��DV{��� ?R�R���g;��p�,��5��R    	Q     �  �    4o   ����e�X�����D3�"0�Sn��            (�/� �e "G#�)	χ��p]6'S�N��$�|u!fF�^Q������|K^ɧ�C6N�F�t�
o#m�4�I>|8H�D��ͩ�(l�D�Ft5���8�����`�3�� 89W�Zl��J% Xm���ZuQm��B    	�    6  �    9�   ����!��B[Y�b��w%�l� ��            (�/�`�e	 �3-��3�0'p�1`���9���-��wϕ���&*�-�1=M d"�Gԝ|��\�@|>8N6@	U����%������/�wơ�����j�߁�
$T!Uƒ�b<.{��E=�E�.ȿ'�e�rj�$;�"����Į�h�V����H��р�s$@ 6���4#t��7W6bZϪ5MS4��ĝ=�%��1��'�@Ģ����% P���G�`�cc"ሁ��I�v�y0��/IଫI��k�,GӴW�\ �� N��DV{��� ?R�R���g;��p�,��5��R         D  Y    i   ����K-�!yBQhO�Еw��Q�              �  4   8  virtual void RenderLayer(const nsIntRect& aClipRect);
    `       7    nT   �����߀��("�-+�ߌ�@�                1      �  �        x     �  0    �C   �����6�Z�h[(���r���0�f              a  t      �  _   l  virtual void ComputeEffectiveTransforms(const mozilla::gfx::Matrix4x4& aTransformToSurface) MOZ_OVERRIDE;
    �     U  0    �y   ��������_���=�u�`��i�            (�/� Ve $  =  �   J  virtual bool SetCompositableHost(* a) MOZ_OVERRIDE;
 ;�qBf    Q     d  4    *   ����$��������VE�|TJ�|              `  �   X  virtual void PrintInfo(std::stringstream& aStream, const char* aPrefix) MOZ_OVERRIDE;
    �     y  �    .   ����Y�y
�AP9���
jӳ�Id�%            (�/� �� �  � C  virtual void GenEffectChain(& a) MOZ_OVERRIDE;

  � +  gfx::Filter Get();

private:
 3x�w��/�<�,    .     +  �     �   �����؀�F���K;v&���eb              �  �   protected:
  �  �   public:
    Y     J  �    <�   ������	3;��a%��'�����U            (�/� M d  {  �   A  explicit ImageLayerComposite(Manag* a);
�G��\e    �     �  	�    n=   ����g9Ɖg���O��dQ�V		�:            (�/�`L M 	#*pA 0�D   [�cz�Й�C��h��R"D$��&*d�1��c���C[S�>���� I���iA]�� �:���ʭ����t�J�٬������ �4̘�9�V�Qt�R�^eHN�wz�B�¹�>n�MA�#s `��I���b2�k��E8Ej;!���RUd=�al��
DL�v�qB
��{pD    v     �  	�    ��   ����S���/l�jP��+ʳT���            (�/� �} 2�%�I ϋ�<��
R�?������Nb�<Eҫ�,�����e�"��$�Pjs�w��q���lK;���$�ĴPL�;س �`R�&�2���H5rj�[�u�^�n�6���ȶ `O v ���p (��m(          	�    �   ������@q�էC}6~".�R]/F            (�/�`!} �];@ �o]�w�?IUUU�_Y��S�p�s�ig��m4
]>�Z�kf��#����'�T�$�yM L M ��?��K�m��歅��B�L�f��T�_��~�j�z`2��'A��2��%�2Wj�\͖]�X�@�_�?�Ds�k-�*�e�/J��@������R�}�q��Ϩ�OT&�?�fEu�6�h�;��^2�0��?���׾��2R�'N9����ҟ"3�E���^������t3�����^��v��)�|�jX��XV�b�)c���Zk��q�s��Q��-GP����r��s�Ѳ4)��J?�0�%��ݨVyh��+���?B�7H��B^D��|��rZ�(�j)�5H?Is�^A��@`@� ��� `�Q��  f��l�F`�T:�ՊΖ�al�a��� GH�צP�e�[Έ�n���zȆ�e��B��
ooc׿7�=	\Gmq���c�aʂ�.��`>�x�r��*��d�#�x���v�T�*���R��    '     �  	�    �{   �����K��Ux��n�l/�섵:                   #include "mozilla/gfx/Rect.h"
  �  �      �     D  virtual void RenderLayer(const gfx::IntRect& aClipRect) override;
    �     9  	�    ,�   ������4��ѕ�e���e��/              	d  	�   -} // namespace layers
} // namespace mozilla
    �     o  	�    k   �����#���y�s��R��@�;            (�/� y5 �E`m ��1^D��jF&`�8� �����J�y|0M����l�n�]���~�������n��V<�`h��6Y]�t���;��? W�
԰Ł2    U     l  	�    k�   ������T�Ro�%��}��+�'            (�/� u pK�H�I�Ҧڂ`��q�gQx���&���c%m��~��*Z�Ǐe��3�'l���P5M#�L���"�
s������<�� 8�j0�@    �     o  	�    k�   �������J�����������p            (�/� y5 �E`m ��1^D��jF&`�8� �����J�y|0M����l�n�]���~�������n��V<�`h��6Y]�t���;��? W�
԰Ł2    0    �  	�     k�   ������jYPf�^Q�,jǙ����M            (�/�`�� ��' �6�A�NB�2W�(Y�L�pnu?�<_�#<��� �y � | ��-.m_�=YzG���X.���G
"�p��	�1�1��,W�b�Xy��ߩ���Bs�O~%��W����W�|��sf��;ߋ�S5��bXA�F�p%ȏ
ަ����~D@ y[)Vy �jc�Jp/�Ef��?6
�MJ���`����y��X���^��e|��L��Ww6Cy �J� ���V
`�<�UE����F�	�����J�q c��T1�(�
��1RAP����~a<dmM<T�+N����S�rȨ �*�'�j�?�<��ы9���B�d�K9,p�>|`�^-�#m��(�c�SVg��Z�6k]��2{Xo��9�r�e<�_���J!c=#�R�@���,L�2��IZh����l-��Lz�@,�`L��H�E
r�b�m尴:�VZ��T���lM��N�� l��N.�����\��p@�I�aνXԥ��&f�����Jk���M�2�n��5l_S�@�Ik��yO������!CHd�    su`$�2�I� A Y���Y;.���_@���j��`X',�u���xA����� ��9=���D� �0 �V�*��;�)�:|��t��y�(��<�@�{���u	�T�b�S���5�⻳�0���q�U7'��1�FЍ�$5���c
���-�e�k�N�« e��Ǝ��#�]H��5�uy�c�����'�4��*�ùrvm~���U�B���,7=h���B��>5�.Na���TM�B���9�	;�at�D�L?w���k@<����G>j�'a�dJ�9+�w������ �����ޖ���fĒd)�S�&�_	J�k�-��$���a&	��#�s���]� �����	�M�t�,�    �     o  	�     s;    �����8%XT�䴏�����.'            (�/� y5 �E`m ��1^D��jF&`�8� �����J�y|0M����l�n�]���~�������n��V<�`h��6Y]�t���;��? W�
԰Ł2    ]     l  	�   ! s=   !����Ad�ѧ>�)�\?�榩o��            (�/� u pK�H�I�Ҧڂ`��q�gQx���&���c%m��~��*Z�Ǐe��3�'l���P5M#�L���"�
s������<�� 8�j0�@    �    "  	>     �7   "������XWp���3���ҲD��Q��            (�/�`n� V�{<@k �?@6�{�����6�kE�H��o�� xJ���gO2���5��!^��
c3�e7)�o h k P�f�g4�T��.�l1,y&�IA�`�ٚ�nF�ɤ�k�y�3,�PJ���9Y$9)��3��ݛ̾�8�5%ol�2Z��(����<,�>q��CQe�AW���<���K3H_������/!7�0��&t䢉�<X�;�6k�L�T��5 ��OU#��%FQ��Ջ�]s;����_��'�0��3�M���s_p���)����lg3Z�@�%�N��$�IGU��!��d'�.?�����m-��bh�"q�89�kQU�њpe�M�[V���B��1�8�����:Y$�	c�L���)@�w"?��%Ծ�8�E)��&_; �w��,��9�kr�N�b���j�Z�=gU�zZ�=�̀����ǘ��&�Xcz��L~��B/rK����_A�S����
e�,��΍^6~�aB���d(0@sg`ȐaL*�=Y�S!��:C-��2�\|�G6�#�\�[���w�9T����	O�lp ��Ÿ��Dޟ��pE���k�?��0m�[;S��`�+�e������Dq�;��C�D{Z7Ge�b�l:#,&`ͬ�����-��ͳ���a@���l;V5jVZo�"�Ҟ�Zޔ�Y>�X�����|���4HIPFW�%�6��0�s&��W�t�i��L��),q���^7R�~0��Q0�i��ÄH�(?XB�w���>d%��A��    �     F  	x   # ��   #������Mx���~�i_s3�%v��              1  1   :  virtual nsIntRegion GetFullyRenderedRegion() override;

    1     7  	�   $ �   $����%��g�G6[TP�S�j�RJ�J�              �  �   +  gfx::SamplingFilter GetSamplingFilter();
    h     x  	s   % v�   %�����	]�e�B����g9��            (�/� �} � �� �
 DZJ�<ԓ�t�@��C�0Lza��e�b�([�;����6����hA&U����R�������;,&Τjg[��l v$� \m�����>3g:�    �     x  	�   & �G   &����(N��J�q�n�NTW�`�2g            (�/� �} ��`m_�f҇�4yҞe���PppԀ��r ~��I�ީo��C�)0���጗
/��Z�tӠ?�!G҈� L!��eb��'��+#m��'�x�Q C�� p6��b    X       	}   ' �
   '������%���-V2h����͟�                F        d     �  	�   ( ��   (����{���<�(	�$ >yک�B�;            (�/� �% b�$#�����{�L����i��S���l�Q~	H��J)�������`ɞ�&�����/��g��l����gΪ,��=}�=����0�w�עOQ$㫓�d�SnN�#Q����(v�U3	=r|)�Q���B��pWI=	!�  S@0��$�E`          	�   ) ��   )�����H���m= ү��C������            (�/�`�� �[Z9`K� Y�����D��U#��ɒ��.SRz6���+L���B*jM����&43GE[�n>L J J ���H�7D��+p-����ƿ6��[�m
�{�8)c�*��],���a6�$�2՚�\XH��(�f$`�k� �������%��Xc[����?�?���+e��:V:I�"3#7T1���k��Ikr�(��NQ�|�L����Z6�A� zq,B���Dnנ5̴ƺN�[Ub
�t|�*�[lSQ���j�c�D�����D�o������ǿZ�I3���>7Օ܄��� ���G ۮ�<�����p��voI �"�[�Z[��NP8)=#W6�9<�
�)(�; 0�*�ZH�Ags��.e��Ou�<A,y���*�Uq5ƂV-�B2a���K�N� P�M.� � �fćXL㋶��=����xh)@<�nDפo��CJ��bp���2L:Y������o>������������~�A�        �  	   * ��   *����^��ڎ����;L�g��,            (�/�`Y� W6P  ��T��ڟ���N�n�;Ɯdv�Y����?5���1" � �d�r�+/3i�xF J H ��=4��J�������*���_��7c�EHsE�-��g�h���ԓ�1Q�'�0N�5P�E������w�?��p�Ƽ���pk�_p~��d39�����ڮ8Q��m+4jd.zbJC�Ñ�,�	��$S�)����?�JN��p+�BPI�e�� �7<dA�]Z�Ԛ�V��@%��5<fu���r�
ϟ��<I-N�d\� �C<?���d�W��l�����eO>���z�XAI�''�;@F�v]��ϚP���	�%��M�,�π��`��D02 0����q+o�ty�͵�=`��h,a�eb�^�@ Q`�-���8'G n� ��,^�C���x���/��oi���ɰI@5�����l��{}y��؂���Y�29\���6v�Y��