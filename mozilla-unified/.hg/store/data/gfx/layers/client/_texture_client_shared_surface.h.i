          
&     �P�������������L�
��u&pO��{�            (�/�`&	� V��(��V �J�S�+"���o��#�����@X���(u�\� � � �T��S��U�Wx�x�շZzrg!i�J����y��a��=n.H�:^�K�IΓ3�u�� U"���r��~��c�Pv�/[ΰf��f����男�S�7�jy���2%�,7��Ҕ!����JK��/�S��-�)S�Ȟ�GSUP���-ߋ�$,g�m��yͯ�&���M.������ܴf8���J�
��l���JطA"�m���/D���S8�l>*S4�&�q�TT�}&4NF��\h>�����dPH<2�Q�6Q.MR���HL"[�k�(��z�36���^q�����91��!ei���%�GD�|�Ǟ�������r}���H�v�3I+ޗ��7
r?�j�Ҏ�z�H��l�� [gU�{�|-�S��s�����lmL�w��sEn:���V쪙�Z����h��Ad�t#���#� 7�j&���QB� ���v�jW���H
9�3
��I�\$RK�;�άZ�dQa<�����93J��X7�%+n�6�7�Ȟ�u�b&,(&"&*(����.[?��r��t�r��uws���1��ƫ�_8�}j�f��3��rG�@���ZC�.k+;F�)1����BF` � ) 0@�bf4 bA�"9�I#P�� � K
S�  �~�e����@EP2%Oc�Ҥ~P�̘��0u�˩��(VV%.�Ҭu�I'z�O� O��s,��O��,�Ddꓺ\"ͧ5�t �r�9��������&ft�e�Ů�F6F�L�g��1j%M7^��c���d��Ƈ~��!R�V�z��k�=�|�����[#�<��1'�G5	Y�Ѐ>�å�*�I�|��@-����M_�װ����Fj�ԯ�`+#{.�uK��qN�*�H!V�-��Ɂ�o����rwʆ����ju����$��@�#0k�m�+!G<)���GZdl��@:~]*��j;��*0����8*oTZ'�k�K9��           	�     ���������6�>,��-�*5�_�5	}�              �  0      	�  	�             6  	�    $�   ����������A�pl4�M��[              G  m   *  virtual already_AddRefed<TextureClient>
    T       	�    ,�   ����l>p
9��:��g���'�              F  H   } // namespace gl
    r     ~  
�    ^f   �������!7!#)��>��J�v��            (�/�`@ � 2��9$e���%P�H�`�Q:! ��Ƒ(.���+��>�Q���W�axU�38���O�peGW��ed�*.�P � ��ivh|l5�%�� ���C( ���-)�Gk���    �     O  
�    k   ������X�n��~���l�i�            (�/� P5 �  �  �   D#include "mozilla/nsRefPtr.h" // for ,Counted
 +A 5q�    ?     M  
�    k�   �������c�+
�|A�Z�:��=�            (�/� N% �  �  �   B#include "mozilla/RefPtr.h" // for ,Counted
 +A 5p�    �     O  
�    k�   �������@�dl�\G�gb�f�u}            (�/� P5 �  �  �   D#include "mozilla/nsRefPtr.h" // for ,Counted
 +A 5q�    �     M  
�    k�   ����@cN\�bg�����h`�P��            (�/� N% �  �  �   B#include "mozilla/RefPtr.h" // for ,Counted
 +A 5p�    (     O  
�    s;   �����_R�c˩:d��L/:��תg            (�/� P5 �  �  �   D#include "mozilla/nsRefPtr.h" // for ,Counted
 +A 5q�    w     M  
�   	 s=   	����<N8�XJ�W��vsi�r"(d            (�/� N% �  �  �   B#include "mozilla/RefPtr.h" // for ,Counted
 +A 5p�    �    +  k   
 ��   
�����c�-�
��M3���O5��~            (�/�`� [Y9@�C (�y��J��UK��*b2�w�3I���$.��ƙ=��H�'+N�?�w�� t�6$�F I K cI���x�����?���ٵ�j࿃����2�6&G)�r/JE�{�Sz���zf�:��4�c�'D�M�
{JnQn��;Q�_���d�N�E�9����b�зS��n�S�o��A���EE��sR��4��<;\KATȳ$kl�#�gI=3
u��X,����/���:J�k6�e���5g8���K��3~�B���bG��xz+*z��3c9�<�-l��Br�e�*rZ�q�?��;T �DL$�>:J��	����TKܠ5	����o�9�rzVW'�>���*I��)C�
�( H)���f�04�,!,�  4��k� ���TGS��_>�2�?J��g�`Q�0Fw��F}2q��1&2;b��*r�M//F�������G��`$��$K�E4��[cN�{�4��?�C��L�m����Ë����G���W�pŇ?�%U�!��~-�)a�R7)�    �     D  k    ��   �����Q���<M��h�	8�X�8�              �  �   8class SharedSurfaceTextureClient : public TextureClient
    	3     .  �    ��   ������.�O�n�Yݳ���ħ��8              �  �   "  ~SharedSurfaceTextureClient();

    	a    �  
�    ��   ����[^�띝�8�F�,��?�'�            (�/�`e ��W:`iC ���k&�y��޻�F� ��۝I�������RC���%M>JJ~�-L?!Zڮ�HD F L �ry���ȿ��M1K��6��+Շ������R�eL�F�pݫ,��؋�X�sj�L3���Q�Ox���
y��>�m�y;Q[�H)���}1f�c��uT#��9 �Au-	�tR�:!�]�Ӂ�tA0��,L'�Q���pS�KiT���eu�.���ބ0���s7m��Ꚑh[�Ek��Ɩ;� !�ˋ|���E
��Ӏ�
V�J;�K���LĆ��� ���][�~�$?ɿ�����qL�E/*�K�!�K�C�t��v��,��f�LO��b6 0CH)��A����fH�=v(j+��;\L�'�K�:�D�%� �����)h��۶0�e��qp��J.������"l�a1M�$����Uzɺ@X��M��T�M��C\Cdrϒ:2LnhNu��
    W    +  k    ��   ������l�5�����3�+
���            (�/�`� [Y9@�C (�y��J��UK��*b2�w�3I���$.��ƙ=��H�'+N�?�w�� t�6$�F I K cI���x�����?���ٵ�j࿃����2�6&G)�r/JE�{�Sz���zf�:��4�c�'D�M�
{JnQn��;Q�_���d�N�E�9����b�зS��n�S�o��A���EE��sR��4��<;\KATȳ$kl�#�gI=3
u��X,����/���:J�k6�e���5g8���K��3~�B���bG��xz+*z��3c9�<�-l��Br�e�*rZ�q�?��;T �DL$�>:J��	����TKܠ5	����o�9�rzVW'�>���*I��)C�
�( H)���f�04�,!,�  4��k� ���TGS��_>�2�?J��g�`Q�0Fw��F}2q��1&2;b��*r�M//F�������G��`$��$K�E4��[cN�{�4��?�C��L�m����Ë����G���W�pŇ?�%U�!��~-�)a�R7)�    �     D  k    ��   �������	���
�$�c�yǜ�*              �  �   8class SharedSurfaceTextureClient : public TextureClient
    �     .  �    ��   ����9��C&$B5�л�`��!7�O              �  �   "  ~SharedSurfaceTextureClient();

    �    �  
�    ��   ����T����/p���9яjyOy            (�/�`e ��W:`iC ���k&�y��޻�F� ��۝I�������RC���%M>JJ~�-L?!Zڮ�HD F L �ry���ȿ��M1K��6��+Շ������R�eL�F�pݫ,��؋�X�sj�L3���Q�Ox���
y��>�m�y;Q[�H)���}1f�c��uT#��9 �Au-	�tR�:!�]�Ӂ�tA0��,L'�Q���pS�KiT���eu�.���ބ0���s7m��Ꚑh[�Ek��Ɩ;� !�ˋ|���E
��Ӏ�
V�J;�K���LĆ��� ���][�~�$?ɿ�����qL�E/*�K�!�K�C�t��v��,��f�LO��b6 0CH)��A����fH�=v(j+��;\L�'�K�:�D�%� �����)h��۶0�e��qp��J.������"l�a1M�$����Uzɺ@X��M��T�M��C\Cdrϒ:2LnhNu��
    �    +  k    ��   �����X�C�tY�d[����Nhi�\            (�/�`� [Y9@�C (�y��J��UK��*b2�w�3I���$.��ƙ=��H�'+N�?�w�� t�6$�F I K cI���x�����?���ٵ�j࿃����2�6&G)�r/JE�{�Sz���zf�:��4�c�'D�M�
{JnQn��;Q�_���d�N�E�9����b�зS��n�S�o��A���EE��sR��4��<;\KATȳ$kl�#�gI=3
u��X,����/���:J�k6�e���5g8���K��3~�B���bG��xz+*z��3c9�<�-l��Br�e�*rZ�q�?��;T �DL$�>:J��	����TKܠ5	����o�9�rzVW'�>���*I��)C�
�( H)���f�04�,!,�  4��k� ���TGS��_>�2�?J��g�`Q�0Fw��F}2q��1&2;b��*r�M//F�������G��`$��$K�E4��[cN�{�4��?�C��L�m����Ë����G���W�pŇ?�%U�!��~-�)a�R7)�         D  k    ��   ����Y���P@�#�ei�(��X�              �  �   8class SharedSurfaceTextureClient : public TextureClient
    Y     .  �    ��   ����L�u ���A��h^0{j              �  �   "  ~SharedSurfaceTextureClient();

    �     T  �    Ó   �����F����V�k���e�3�                L   H  virtual bool HasIntermediateBuffer() const override { return false; }
    �     �  �    �/   �����Y!|���8[�YOc� {2�            (�/� �� �k  !$L�&�;;^��f�����f$<"?���&)J��\H��AT����X�YT"����G�՚>�*И�I \C�> u�`���{��pm=��_�� Y	 �M�{E�KH8栆	    _     �  �    Ӈ   �����ߏhcK3�A-F�e����t            (�/� �� �pk��#3%��x�o�Ezzl	��M��|�J���#��u푋�!�����vD��ͨ|�n>�XvH�	@6t�*&��b�z?��#�� X��P�D�V�/�c�4�2    �     �  �    �v   ������v��AN=�ʯ�R�� 3a�            (�/� �� �k  !$L�&�;;^��f�����f$<"?���&)J��\H��AT����X�YT"����G�՚>�*И�I \C�> u�`���{��pm=��_�� Y	 �M�{E�KH8栆	    g     N  
�    �   ����3 
Ϊ����$���ّ                �   B  virtual void FillInfo(TextureData::Info& aInfo) const override;
    �     �  
�    Mk   ����p�_�<u+D9"	MɽsB,            (�/� �% �%�) �
�9A"�~��#��p����Y�s��D��B�γڔ%�)7Ү�C((��4�Y͆D$��0����G%֭�qV)*�@��G& �����?ܬ\��8�?Y~�� W1 ����a�0� 3    B    <  	�     b,   ����yp1�,�/i�CRϧ�l�            (�/�`�� �\]8PiD ���j��>�&@4F~�k��5���?q&�v�(�ZH�!~a����r�	\�bL O N �V�	�kJuI �8~�ܳ�8*�έ���ݣpW3�3ܖ�
3q�F�������4��6�m9�1���~��h����5:���h��[JaW�[(O��;Q����d�Bx��9lA��b�з�l׉@��ZҊ@TWR'��6��\1pPæk��`]�/���| 6yC �Q]a4z�^P(����s[|o��jb����n�+����C���p�+2%�M��Ib����\�_��O`�����	/��I$�r[E濄���4�t�m-n��H&��4=Tx��~��͝�z�������肬F,H���a(T� j�b�R{�UF�&B�%�� c�X�k�IpV��S�f0�˫A��W���bH�v��40L:�O� ܧb��cUlN�u2
�Qp���V�c��1��Є�/��`�����=R�A��b�|�,�+�7��b�(fMH��V��Ē�H�Ըtqa����>X7�f =��Pc(�    ~     E  	�    b�   �����r�p�A�uG�a�5?              �  �   9  virtual bool Lock(OpenMode) override { return false; }
    �     �  	�    ��   �����O�� �tEz��N��            (�/� �% b�$#�����{�L����i��S���l�Q~	H��J)�������`ɞ�&�����/��g��l����gΪ,��=}�=����0�w�עOQ$㫓�d�SnN�#Q����(v�U3	=r|)�Q���B��pWI=	!�  S@0��$�E`    p    �  	�    ��   ����@"�o�Ѵ��w�/��            (�/�`� v#wA0�(@!�`��Ҡ��\:�^ѳʟ؝�Ui[~���(�MRļ7��o٤��Y2�ۀ��D�ĆE>g b a ��d���|x�*���)M����S��+H�Tl~U�4i}�M�w)e\6�F]�M���@P�F�@S�uJM�֗j�M�}�rO�y���o�<޺�����Ŀ!�QN��ml����mla){���A�9L��M�G �`��'����m�6^�i����� (����j8b�#�l���̚�.U�)�H7���1�*�"�"�H$�0ܐ��:��9ƶ��Nd;ዲa��:�Q�kT�T���c�ӂc2��t]"Ы�$8?�[T�'MY���O��PX����o��S��;rWkzT�T�湬�mx0�#�F<����@F!����ЬP$���ɍ�Ł�,?J���忘� *��LH-75s`:}i���0j�vͶ���?�����q��#|g��:��$qlJ��� 2�& �[@cP�up iR�&"ZX ���Z�(:`� G/+�F�ͺ~����{�I3n�BOj�p1R�B���(���2�.���l��90�T�V�Oڨ�߉�n�ku�<N�q��5���6n(��JC�����u�9b��
X�2㎃��I�dTǢ�������)�VW�v$�T         �  	�    ��   ����d$�ʙ8����2����>}            (�/�`c � 2�2-�K�g�K�K�����\��}U�R%�o��x�P�@]9"����
��o!ԊD� Q�:a��{�����c4��hB���F��V�=u������7�HAn�hBx���0� ��;p�3������)9�d>����"#K=�v�s"Wr�{�lVb�fi<���Y��5� �0��lWb���U$mk%�ȧUlw������ x�ng�5A^3p��C��F�ʞ�1$.e�Z�_�
f          	�     	'    ������mV4/,N�8 ���0�ў            (�/�`�  �22`k� ЯG���u���/m��}m�؊���ġ
d��p �"�|��p�D~������K��I'�vq��^z(ɍrf���6����tP۸���?A��2E���A��x~�(�X&Rܨ��ێ|��3NT�ܟ��=�d*+�j��L7fsS���%q�tn����I���5�J��6KR���8J�_�gxK�w | c�1�ų�{�
�p[�e(�%s�2���G�V%xKY�VO*+Q���r          	�   ! 	E   !������,�*|��(�w=+��            (�/�`� � �N1/`k� �J�}���n95=�B+����tF	.�9�� ����#2~ʖ�=��S�_�"G!d�@�o���N�i�$��Zeբ2L\���zYY�V�I�
�<�?��η��"�zI�b�#ry�٦�; _Yb�qo_��l7�kU֕�h��ȷ6U$�ճĬ5>��$]�I��,,L"W�a��&A�J3)�� Ac�L��LM���F7�p�Y��Y�a��a�5���MGu�n����          	�   " 	 #   "�����B��YRr1�K�({6b:            (�/�`�  �22`k� ЯG���u���/m��}m�؊���ġ
d��p �"�|��p�D~������K��I'�vq��^z(ɍrf���6����tP۸���?A��2E���A��x~�(�X&Rܨ��ێ|��3NT�ܟ��=�d*+�j��L7fsS���%q�tn����I���5�J��6KR���8J�_�gxK�w | c�1�ų�{�
�p[�e(�%s�2���G�V%xKY�VO*+Q���r    (      	�   # 	 5   #����ɚ~M3�TQ��ϖʈ"Ay�&            (�/�`� � �N1/`k� �J�}���n95=�B+����tF	.�9�� ����#2~ʖ�=��S�_�"G!d�@�o���N�i�$��Zeբ2L\���zYY�V�I�
�<�?��η��"�zI�b�#ry�٦�; _Yb�qo_��l7�kU֕�h��ȷ6U$�ճĬ5>��$]�I��,,L"W�a��&A�J3)�� Ac�L��LM���F7�p�Y��Y�a��a�5���MGu�n����     )      	�   $ 	 [   $����,j� E����s;�8,��            (�/�`�  �22`k� ЯG���u���/m��}m�؊���ġ
d��p �"�|��p�D~������K��I'�vq��^z(ɍrf���6����tP۸���?A��2E���A��x~�(�X&Rܨ��ێ|��3NT�ܟ��=�d*+�j��L7fsS���%q�tn����I���5�J��6KR���8J�_�gxK�w | c�1�ų�{�
�p[�e(�%s�2���G�V%xKY�VO*+Q���r    !6      	�   % 	 �   %������T1����ؔ߯�����            (�/�`� � �N1/`k� �J�}���n95=�B+����tF	.�9�� ����#2~ʖ�=��S�_�"G!d�@�o���N�i�$��Zeբ2L\���zYY�V�I�
�<�?��η��"�zI�b�#ry�٦�; _Yb�qo_��l7�kU֕�h��ȷ6U$�ճĬ5>��$]�I��,,L"W�a��&A�J3)�� Ac�L��LM���F7�p�Y��Y�a��a�5���MGu�n����    "7      	�   & 	#j   &�����ɸ�H*��b��N���ϧ�            (�/�`�  �22`k� ЯG���u���/m��}m�؊���ġ
d��p �"�|��p�D~������K��I'�vq��^z(ɍrf���6����tP۸���?A��2E���A��x~�(�X&Rܨ��ێ|��3NT�ܟ��=�d*+�j��L7fsS���%q�tn����I���5�J��6KR���8J�_�gxK�w | c�1�ų�{�
�p[�e(�%s�2���G�V%xKY�VO*+Q���r    #D     �  
G   ' 	O   '�������%i�d(yR���&{�            (�/� ��  pM��?�h'��d���'iğ���;��4��ks}g�=0lhY�'�Ю՜[�8��D���A���?*�����2��5����;Ǧ���.) H) p�"��Jy�
ޙ�=8"    #�     �  $   ( 	ei   (������,f��㦹U�Sl�K����            (�/� �= b� $�93�0Ü�F��$ɨ$)�(;�e��Acf���e�R��7	as%����OB�uJ����F��Y����CEҺX6�q�����'�a&ou�=�l������qd�SS�0��v�\Өܗᵽ~OՉ"(J v���&b��m0wW0G��Ry�����G    $z    �  �     	�   )����~>���"i������4�VL�            (�/�`� �6�B�  ��*��" �m ��H����nG�}��M~�ߵ&\sFթm���gyT-� �� �?���f�� � � ����m���������uQ������ ����+�#\�#��ʢ�˫���WPuu+\���mf���i+\�0� �6oH�� ���.Ƃ(��\M�b͕�&G���8�z"�,NU���M��x,��b�^��l1���r4��tH���cƴ�FmL�괵��]�s\��ɲ�Z�YOj �_D�?�_tvR�r�ԩ�U^�Oa���~�=�v'^�����,�p'��fƱi7�K86�L��	��@2"'lʵ�:���?���4�&��\�-�^V�[*�N�3+�\4��t�CAK�}V-����T�k��ay��2�܌�	Ƃ6K�T�v����ذ�uM!Jf����E`�02t��f�����R�U���v󶞼8ڼ�ۼո0܏i����96m(��=���u�5�������xX�UNm��*���&�I��۽2lk�}�B�?��Op�_���HJ��F&�SHd��R("R)�LPD�T�QH
T*�$��pp�|����_a��K��/�aM�)�.h �p�aZbخ�?	�/b#Q����U*od���b�4�l����7`���P�ˏ�;��� ���������y�19��$)F8��d78Qv�K�z��ǭ|�A%3C�)"-�@ F��� (�b�FDF��Y�%$�<�6�$^�p�̘m�����/w�]�����9|X�3�0.�4� �C��W/��cP>��'rTJ�,W)��p*�b�����l�;�=mbY?㤵
���pL�������e�u2�V�+�a�I�굗}��8Ĺa`�5���}̡�X �8�V�Y���� ��݆���n�w�H�"+8��E�#T�P���5���CZ��9, kV0&��C��I���wEǻi>8��� �@KX�mq��T�yT�
P�$q�