        -  �     ����������VE�V�f���s�LP}�s���            (�/�`� v�d%��L��r�R��5��m�����B���&��  X�[ Z V ��>B��7C�g�s>z�pl9�7��9!�3�r������}G������̕�F�P�����`��Ԩ���F+6�C������>D@ ��mx�i�q��0~dKb����!hä���m"�yH��{u�����b�V���	��N����k��^=/�&i��)�7����Y2�t�4�r./����71/t�*������K��]���*\���(��M[���$��>�B}�&�����Ӓ��m��й}�y��+EɈ��8s�0�R�j9�e(�P���M{�t�B%��J"��p\$�CSH@���AkN �jt�K'I�(as[wMn�zE��l�OSl��s&w�_=Z�-I)���9  B��7w�L P��Db.�ݻZԍ �:�,�D��+	A&/D�P,_%A��:H0���6�ո���'����^Z��Vm/���g���`� i�i ��E;�#ӭ�7��Ű>��*�΃���X*��Hf���z    -    �  W     !    �����kF����[s�����&�W��            (�/�`OU ��C.p'��U ��IJ�-%Ƅ>%J�)��PD��7ò�3:�[��B28 8 6 ^%�S�1k58�u8���6T���З��jt��A%�/?v��;�ZsX�׫��`���HʼPL��vL�*�/p#[E�Ȫ �P/��d�@a�y��k$�V�ѢH0�M@?%�#*%�$𧓅޵J�Q��~������rR L�;24�������nO��P6��@6�>M���Z2�6�>
32�;j��R��-�O2��jL�_o	l�ν�n�0 ��y]8�cX���D�a �2��5����@���#q�������K�=����4��@SZ#���� ǻI8xm�(�e���w^��,�y� �����~�    �     2  }    �   ������;T^����-�*�X�:               �  �   &include "mozilla/GfxMessageUtils.h";

    �     Y  n    ?   ����ؾF=����9�
��4Y�[u              �  )   M  async RootFrameMetrics(ScreenPoint aScrollOffset, CSSToScreenScale aZoom);
    L     S  �    �g   �����#�_��n����6�X[��            (�/� ]U �  �     Q  sync ResumeAndResize(int32_t aX, YWidthHeight);
 ?�����    �     8  �    
j   �����(�3�R]C�gʵ��-:���              r  r   ,  async FixedBottomOffset(int32_t aOffset);
    �           ��   ����S��
��Q�����n�V 2��              �        �  �        �     U      	Jj   ����2�䬼�n���]¶w              �  �   I  async ScreenPixels(Shmem aMem, ScreenIntSize aSize, bool aNeedsYFlip);
    D       )    
;�   ����lCN����l��H09�{Ǫ�              �  �   [NeedsOtherPid]
    `     +  8    
[O   �������A��j��\�G	��              �  �   [ManualDealloc, NeedsOtherPid]
