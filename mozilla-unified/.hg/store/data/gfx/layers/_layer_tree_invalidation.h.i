        �  {     y�������������.v?�D�S����]            (�/�`{� �r�& �� �;F'����Җ��Tr�ە�Ȭ R�CUT����%�� � � ���Ø�[�_�v>i_�;�?f�L������Pdph���m1m�te�έǃ��#j_���w�銏{|��Y�^��q��?g��r��F�z��1%J{�-s(Qʦ4��������C��_! ��@�q�Cy�Ф^��i%�A��vN�r��Py��Q�h)9������������X�9��Y��L�ǘ�1PAjGb ���o S^J� �����T(,\0
�D�f�C��hp8�S�X��
 ����6�A�(:���w^��7[z���@���k�f��jֳ�y��g��H�]^w�g,�n��"U؏�X�R�N0-�k��K�V_ՑG݆j=h}Ҫek�W�H�/�ߑ_;�䟈����o�[�.�j�E<�O�6��hi'��k����ۍ,z����x�S;�0Ea��*G����[�r����^�0!�V��ɤKmS+��d��M^�n��m�YuBe�|�|���|��y�z����T��M�����,�vv�Y�r{y����յsj��ő_���a3H ��   �r
{`b�T��1� ��{m�$,��d�(6jd��t(FY�h\c�������X6�����5�o�&E����Dx�Bv��5�L�!P��@C��|�M�(�K�BG/) ށ�d�|J�!���w�V�]g	���-�5rԱ���'��8GS�����;��L��u�Om��s��*��:G(\��γC����.�{,�I�������U;�8�R 5?���Qu���ec%y'���mջv��f�Fr��6����p�`�t��H5�J������p��; i�K�    �     �  �     �d��������7��ϧ!6����f��[R            (�/� �� � �9 0�{�M>����.�Y����R���ڈ���G�=���� Ƥ���͵q��%N��}�=Xl���Z�B!�[<��5O[1�!u9Ȉ�96$��
 lK�Q� d5F�,���         H  �    �z   �����[����X��~
2p%)�8�`              F  F   <  
  
  virtual void MoveBy(const nsIntPoint& aOffset) = 0;
    P     z  �    ��   �����:�'�p�U�bE@P����v            (�/� �� " �93�����YI�$�~��@eM�x�^�K^����p��c��նfY�pZG1�Edi������	QF0!�RmF��2������ȧב�[G:���kw�] �D    �     V      4i   �����U�8��;�w����$�e_S            (�/� fm "��W ?�)O��C��a��~e[�}&�#Vŷ����ܡ�A���Vt1QoA����� B�z�!�O          .  �    4o   �����
d�D�K �,�����              g  �   #include "nsRegion.h"
  �  �        N     V      9�   �����=ޞ8��d���P�$��ֿ            (�/� fm "��W ?�)O��C��a��~e[�}&�#Vŷ����ܡ�A���Vt1QoA����� B�z�!�O    �     l  e    �H   �����#��2�vE0O�����            (�/� � �E��3H� �~�$�R���aPG���l�ܡ�)H�z´C��՗��\(ӭ���(�:J�K5�<�GXv���|���`=D�p͇Gr�A 	�r	�         �  �    2/   ������!;���(
����NbV�K             (�/� �� t  � 9#include "mozilla/UniquePtr.h" // for 
  �     =  static<LayerProperties> CloneFrom(* aRoot);
 ��P�ULp�=8"    �     K  e    23   �����0q��0���(�=?�B�h              �  �        U   3  static LayerProperties* CloneFrom(Layer* aRoot);
    �     �  �   	 A�   	�������do�	-'�� ��]S            (�/� �� t  � 9#include "mozilla/UniquePtr.h" // for 
  �     =  static<LayerProperties> CloneFrom(* aRoot);
 ��P�ULp�=8"    ]     �  �   
 �{   
����B��f]���>D��%��            (�/� �U bJ&+��L�0��I���D��S�-}����Q�R�b��B �G�#���lA!Ҡ5:Ѳ�z������<�b�d<���?R~�g�,�<���"&�Z�s�Aۢh�C.?J�^(n�fu��L�?'��< �@~�L$�>�h�j���K*Â�>��[  2L��ke�e           �    �!   �����o�+�>�]�𱿤Is              �               f  	>    B�   ����\s��dӯ�y�%����P            (�/� �� ���	�����i��A�w(6�U�e�P��B�VsI`1��⯦1�wg��4� ��0�C���2��#{���J1� ��xYy*'��3�G    �     J  �    �   ������c@9m����	��%#�.            (�/� b �    �   V NotifySubDocInvalidationFunc aCallback) = 0;
 K�    �    %  	�    S�   ����[e �yfs�_3�6vv���            (�/�`� � �Q8)�9kR��j(�e�y��%7�L>�ĦYW.ȑY͈� ��a�+ + , ��=�����\����etƖ�O��|yI4���dKJ��0��b��W��X�3�S�����5�o{�M)��)p�������<i�8cv��u[� +�v�Ų��I����yb��h[����L5�p́�> ��a,y�Oay=;��pQ��!`nF3G��h��Rˡ�(� 5C|�$�6^��RnA��UvLl�B<v��p
R�Y���7�U�מ���n���    	�     v  
    S�   ��������Ǣ ���5	�����            (�/� �m bF �' �����F&?��^���>b=��)/t,AJYz	A`Vyfݹ�46������dq.�Ae��/�\Q�pq�����H��n��s,+.��6h �c�R�38��	    
g     Y  
    ��   �����6�H��0k׉*+S���                   L   M/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
    
�     �  
9    ��   �����&����k�`5E�5�4            (�/� �% b�$#�����{L����i��S���l�Q~	H��J)�������`ɞ�&�����/��g��l����gΪ,��=}�=����0�w�עOQ$㫓�d�SnN�#Q����(v�U3	=r|)�Q���B��pWI=	!�  S@0��$�E`    m    a  
    ��   ����$�-b	o]�k9>q�r���            (�/�`�
 �G3`k  >�~����Ԕ�!_[���RO�:��ݧ��@7�:�T��2$�Ng�]�M�< 6 6 YA�/zm� �l	�i��BZ$�T����p�M)�H.�P,M���sy
�$`����`L�����
St�g�}���O�G`[�þ���<v�����>	�\����D�˴-�6���p�T&_Rc!����K�JȞ�~%ܥ�J%�/2������ aS�����2O�S���J����lpM��pl�DDV5?aQ�%ZْBIrkt����
߶��W�n �3D :#>9�mue��[�RBGL�Q��.p[6	9a� �p~��*x��Ix    �     S  
    ��   ����?�y���"����'���@�            (�/� _U $  �  �     LayerProperties() = default;
  *  J   (  virtual ~ ½U