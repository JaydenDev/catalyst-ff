        �  E     J���������?=+�{�C*Қb�!��            (�/�`E� v�\&ͨ �Db3&�!<-�	I�3t���{��&D�\� ��
fQ L X #<!S�����Q��a���N|���2����(�t5��C�>�͞H�~z���-��J㣞���RB��a<gD���_����տZ;���ZG�o��׹%#������!�8�y#�f��3�*Ũ-�a:��3-�y���UFP[|�gM�"�L
�d�,$T&�\�r�1e��V�.\�Z	n��)㣴�x�2�6W3wn5e�[	�҅�"�,�K�1�E�����Q�[
7��s�
q<��^��9�5bLBI�@�j���}��J ��wQ�R���΄e8T�{[k����q.�\2��a�s�$ C^	AX�,�iE<B#�#x �(�\[�a���XKa��ĉ6,���~@m)�@���:^U`�-�I(�Yi��    �     �  �     �    ������8:Z�[z��a�]y��*            (�/� �� ��"$���C��?XvzZ+��/�BS;	�]��۬�>�Ŷ�P�Cj��q�-pL�Z�l['�5>��Qr#ͧ���'�/o�r8`^Ky��&x]�u7�����$�7J7-zo�`�-	�1�a8=�X�����U�\%� '�Ճ;    g     �  !    �   ����Έ6��K~R����!J^��            (�/� �� 2@Q� D����i���k��Uԋfc�-������I�IĖ��4t+C��ж	W>�;!*g���G�q���$�ᳫ�$TP;�(Y*t-@�E�k��[v�z
�ǿ ��ns�(    �       �    i   ��������هwG�5�g�O{>�            (�/� �� ��pK|",YZr@���1�ujU���I<��Iz�ߎ�pGs3
�HQo	�b�;,�eeK�\K���6�!�t�Ğ��Ҍ�R�a���RԶ��#lA��c^�S.zDN pN �x|pD    l     h  �    �   ������̧Kr|��u6��'B�A              �  �   #include "nsIObserver.h"
  �  �   7class BluetoothProfileManagerBase : public nsIObserver
    �    &  �        �����٤Qu����w��sE�!            (�/�`� � F�<+p�m ������#Q�DI���z��K�AIC�$Y!�)��*	2 / 0 M��o?A�������K�=%<#K��C��Wp��6/�,�A@!I$ɀB�������YϹ����4�V�|�I�A#�B��-d���w��,��kO�t��v�*E�ݥ��E[3�x�^;��4�����o�9) �W��t���Z[���;�9�����O�����nm%�k�4\/n�Dg�(}���(� A0��"���5��B �'6���a^tHdu�g��    �     .  �    ,P   ����İ�﻿$��L�S!�;�-V              '  '   "  virtual bool IsConnected() = 0;
    (     �  /    D�   �����h��__���r�nX_IBl            (�/� �� ��pk�qE2�j���{qل�M3U��Ө>Hd)	z�Wf�ǿ����2���-�ma� ] 3A��
��p�;�� P��HHYzP�˔aY/��6&��T�C�� 3 0�&�*�ލ<\&.    �    �  	x    D�   �������u#�cGc�<�d�����            (�/�`yM 6�@,pi��zUU��-S�;�:�J/��eFc�1+&�rg�L�4 4 7 ��ԠU��|g��Ϟ��ʲ0l�@r�Dața�í��"�C`UdI�, $I
;j��d�2��aE��|w��O�j*��FƵ��Z~��d��c� g�|So,Z��ޑP�h
� �E��Y���܈=��S��g� �!W~�f����P����
5t�׊䝅���>$��3�	��Q}J������nO��ѲZ4  �X:@����"\p/��)���Sc���!u3D �Gڲ�77V��˰A�,5׻�솨弒�Z����(�A��N%d �{�SÚ�}*	w��A�]��^4VbO�����3̡��E)��qy Y    ;     ^  	�    G~   �����X��c b�,9����e�            (�/� �� D  � �#define ERR_CONNECTION_FAILED "ConnectionFailedError"
DIS"Disc �h#��� n��    �     k  
[   	 JP   	��������*(|�b&l�	x��K            (�/� l ���K3 3�:��X���%f��=��5�1�k�W lx��[A.�h�ء�YA�-m�R.����Z��cu56�Ŵ\��C(�5NpN 5c��9�ʘ�        <  �   
 k   
��������*	��`I��M�<�;X�            (�/�`D�	 �M/*��������$����x|����>;�%	�r�'�P��Fi�����2)/y�3��iY�
a�s3
�Ѿx|�����;en&>	�ɫ��Өz�~{#�5O1�j�I��y�Z���S�i9���/)��#�K�2��n/W��a(����Y�{e"S"���B�d��P���X�Q���`XC-,��} D- @$�Yj2A�9'���z���6�l�������(�̑@��EwA� ��`�䠞 VrP��/��d@� P�5I6Ò� ����`A ҽ�ʞ��T�n� 8`�
��Q���
    	@       
[    k,   ����:��3(�c�����7�m�O              	�  2        	L    <  �    k�   �������G��5��WXM�L�ak8            (�/�`D�	 �M/*��������$����x|����>;�%	�r�'�P��Fi�����2)/y�3��iY�
a�s3
�Ѿx|�����;en&>	�ɫ��Өz�~{#�5O1�j�I��y�Z���S�i9���/)��#�K�2��n/W��a(����Y�{e"S"���B�d��P���X�Q���`XC-,��} D- @$�Yj2A�9'���z���6�l�������(�̑@��EwA� ��`�䠞 VrP��/��d@� P�5I6Ò� ����`A ҽ�ʞ��T�n� 8`�
��Q���
    
�     >  9    ��   ����C��G����'K�ٰ.����            (�/� �� t  
� �  NS_DECL_ISUPPORTS \
NSIOBSERVER 魰a�����    
�     �       ��   ����
�MS�7m�n-m�S�
��            (�/�` M M,*�)� �BP@�>n�]���PwhK!��nt2�1�QMSC��IQ	c�a52����P�>�]���=�'��OI1����ew���)�E��H��&?���[2|@��p{\��0n�{`G���{<�/��Չ�jb�	rC'[o�g��,L������0���uu P W�xǈ*��6�߃#    �     �       a   �����'=�����fp��*Z�%�f�            (�/�` � �P�H�"��+F@��."+��,�t�#j�1��E'�4����2|1nz���z���*�ƴ�wOfxNzhi��Q"FiБ�4&���b���0њ���JY���R�Ň��W�]����  G
 ?���S�zD5`uC˴k*�xzWl
7�9"    A     S      �   ������E�g�!bp���ZՈ�              �  �           ;
protected:
  virtual ~BluetoothProfileResultHandler() { }
    �     H      ��   ����b�K�9� S�[=��Up�&q              �  �   0  virtual ~BluetoothProfileResultHandler() { }

  �          �     S      ��   �����f2KI~1;�[Z�_���"\�X              �  �           ;
protected:
  virtual ~BluetoothProfileResultHandler() { }
    /     �  �    �   ����Z`�l,Mb<�﷏��(F�ʠ            (�/�`�� �K()�)0c��h�e��L	٤ǇXl� �C��g���h��1�J����˜��%�l�+�+�ܺ�UF,7Dz�?Q3
3� 3��;]u��pjb�/�uFd��y݀�&��,�^�iYț����F(qP�a�s
M��S gpL�?{�(Ҡk�;X�U �M0� �˩sT����YRH��b�En|�P�9PŹ.CZ�Z�A�t��I�֜X9pn���d2 »J��b���x��    *     �  �    ގ   ����� \�	@N��U��:�$�            (�/�`�} ��'(�)0c���L�)�"����m�6C��)�h�����)J����ˌ��%�l�+�+�ܺ�U>,/Dz�?Q
3	3F��Y𝮺�R8����:#2O���n B��a�^/ݴ,�MH�y#�0��0�9�&���)�c�����*�4�Z�f f� �˩sT����YRH��b�En|�P�9PŹ.CZ�Z�A�t��I�֜X9pn���d2 »J��b���x��    #     �  �    �T   ����Ԛ`j�s���}�K�y��_#�            (�/�`Ae �
(*���"�,� �)b�xj�E�ʬ�'��-R�$�ԭ�MM�J���U����va`�B`W9�(��r�����ႇ��6B�+�@��@(�4��3�8�hb��S[mun��1�yt�f�8J�A�P�W#�k��[�F�5�1��<G����#�bW"`w`7 9�i��/(͑�< ����ɥ�¹8gc�" E�e    �     r  L    ����������.��?�})���T���>�            (�/� M �`�@�n,�Q�4��RҘ	0 @����`N��|����Y3yQ��0�3���OBN5��-��@�"U���K�ǳ@��Z � �Zv+     k     {  �    ��   ����9�s�'��&���������I            (�/� �� ���� ����_�2mJV�л8�q�f�O��|$0)�1W@ZI���{���iq����+��t���i<f-�+�uIn���:���u��%�X_E�.
�&o���r(% M$^     �     n  �    =k   �����ekJE�Ǡ�kl��K	�T            (�/� �- ҅�' ?��)[cH:��a�}'�n0!e��d���{�P*�����a/�K� ��p	sí\i.CHLiv�#T��E;�T���JM	یc� 䄳���[�R    T     �  �    F�   ����Ť�p���V��To�N�j            (�/�` } �
#&�� <����F�{��$uޔi��ā� �����"Q���N��5=��fuW]�AXy�R��d�P��a��gl3(��D�|H\�lO$vC��@GW�.��°���SW��ejr�k�*�~����7�Ma�j� qkL\����f�:�0v��e�@8g��