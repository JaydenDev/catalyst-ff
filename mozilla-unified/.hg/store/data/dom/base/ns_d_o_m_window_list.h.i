        �  	�      e������������5tY����N��>Q��b�            (�/�`�% *B,Ќ�Bb�����}#}d�B{9��_�����O�W�5�F���/@� � � ��r#Kh�7���������龑�S��>�r����.T� A��TD�q�B9D6ӹ���@�P>���4Q���Ɂ7��!u)���1u��=Yjz��r��G �*d��r�� �C6历���dP�LqJ��T$TFK��I#��!PBB���C��"����0�P��z���C���"��!����� ������$gxV�l�����P�Ӛex>#Y��t�q��t�1~���v�Cp��O��-�~nVy� �&oPQA�.����/*	.Ӆ=ÚnQ0.�eS���p���Y)CN�ڊ(%�̆4���Dd�5��w����e�9�'�M�wɯ۾�T����]ӝG�?��R���?��p���ǵ��\�v֍�B�BY���:Q��S�N��2�l:���ܗ6t[��o���m����u���s���K�R�R1�����ʭ������6���Љ�����)�~�4@y�f�!�a�l	�f.]�S��Hq�����å��v�ٖ��fK��F�Z�n�Ѻ�ܭ�nV��8��0Qt��܅�Px�ɝ�������ko��ok��y��uI%�eB�(쬮��^�(�*�E�PTE��H�ɪ��E�bY`MJ�H0V�?O���£��ɛ��4���@�	�v�+e�Dv�;/1nf��LT�Rߓ�<��6ƖD��m�X��/��'y�Y����d�c%��j���~G�W����y�����C!�`�   �bfv e-	3���"�F� [\a��;R���5��Np@������m���!�����$�S>�a�Ч�n�
����E�����Zd,ƨ@rYT�l���A��\�x�
`�9� R��R-�H#XBۼ��S���mZ�ndir���-
A	�K�v] H�/�f1�,����͎z�7b�.Y�{F/���j�S`tĢ��c�����cp�ɺ=eAN��٠����ԟ���hB4ց+|%��������U&#)0�L ���c��7��?au�^�`��)��b���.����������F*AC c����a��2�Ǿ͸�6�YI��!���Hc���"��m9����a� ��=��e`��z�����"{��/f&�IRB��.U    �     �  Y     r�    �����_��������#ns���9            (�/� � "�'&�'	��W
�Mҟ����S�_&"Mb�0���v���Z�?��%Z�Da$��I	�������MU/����<��6���Zg�ߢ�Y����z<Q���)��(��K�#G,u��\9�.,���㲟���^�X���[�'������I��8 2�b �c����0��.�    f     n  �    �^   ���������d�B�mI��u�Z              �  �   b  // Note: this function may flush and cause mDocShellNode to become null.
  void EnsureFresh();

    �     %  �    ��   ����֔�''aS��
M0��)��l�              �  �     uint32_t GetLength();

    �     �  ^    ��   ����t�@^lb�
fa.əDMಹ            (�/� �� b &�a20 iI�	������hqd�q��վ�G�1fe
S�����j�A�Q��_ƻ�����j��dw�ʒ4�ܜ�0C.oǷ�V�����_���!�̉�$�W*+�9���۸2#e �S����ʭ���=8"    �        M    -   ������� � Иz�[��oxWş�              �  �   #include <stdint.h>
    �     �  x    4   �����=�]B���^�h��7���            (�/�`C } RK(*�9	0� [iAJ�?^28%�~����m1�����Jhdd��b/Gc{�JO��JcQ�{��2��Gv{J�㢯�mP`�D�h�ׄ�!�m�	{��'l���`���2������D"��s�P,�Z�}�_!�;�0��~No] ��X�
 r�J�X�,l{Z�f�:_����^:�1(��U��Y�{0ȃ�    �     $  .    7+   ����V'�ɂ����!䯗��*"�              n  �      �  �      �          �     `      �#   �����ȐK��#�p/M��(���              �  �   #include "nsIDocShell.h"
  �     /  nsIDocShell* mDocShellNode; //Weak Reference
         7      �   �������;7+y~�[��h�^�K              S  q      j  j     virtual ~nsDOMWindowList();

    J     @  (   	 F�   	�����_� ^���M��?G���n              (  S   4  explicit nsDOMWindowList(nsIDocShell* aDocShell);
    �     7     
 F�   
�����s�-f����p9�#�>�)f�              (  \   +  nsDOMWindowList(nsIDocShell *aDocShell);
    �     @  (    F�   ����S��u�F����t=L�{��S�n              (  S   4  explicit nsDOMWindowList(nsIDocShell* aDocShell);
    	     v  Q    ��   ����.$r��B�����	����            (�/� �m ����
��4I���K\J~�-M��PH�1W@ZI���{���iq����3��t���i<f-���uIn���:��ҕ��%�X_E�.
�&oΡ�2(% M$^     	w     M  C    0�   ����x�8�dT	2���N����              �  #   A  already_AddRefed<nsIDOMWindow> IndexedGetter(uint32_t aIndex);
    	�     S  I    �   ����k�%d\k�5��KN�ä�JL,              �     G  already_AddRefed<nsPIDOMWindowOuter> IndexedGetter(uint32_t aIndex);
    
     V  �    #�   ����inv��(�r6���6ߑLi!                   J  already_AddRefed<nsPIDOMWindowOuter> NamedItem(const nsAString& aName);
    
m    �  @    #�   �����S��i�a� ��T����            (�/�`@] v��'�X �� �N%��ƒ��S�I��UU错JƇ����4w | } �K��wP�k���)-*����>g��o��v�5[αR��nU�+���� ��C"���C"��"��|?ir��Iȧ�a������t��PS�T�2��u��z}J@��c�z@H�99���`�@�EMe�{ �E�3�ʥ �MHe�1�ǆ�,+���K�F*0��H1و$�"]2ick�I������L�|}���U��ؕY?k�y��.i������u~�Tp*�;fyDAM�y�u�w7֬Y�|�NIz�Pi[3���G�D�^��ݬ��#�\b���̥�1f�qbU��v��Q�'��K��ۂ4���Q�,��%��`](�RY.���T�s����rr(�w�]�����(���β�̒�c2Rn8Ga8���%?Hҩ�����Y��/�ۃ��=���^��*���]�?��"7䮖�F��V,�vk���n>��$N�Q�$���X+�9&�[��m���������D�l:B��(3$ 
& �� ����#%���	J�L\s�!f����!��lV N�o��
vØn���3Yt�L�V�m�)h.T+������)"zst,����mA�h0Ӆ7� �7܏B~QEu)[GV"��ySc�޼9[��Q3#�Ĩe�󿶐� )�|P:�7� �kJ�X�Y�    B     �  D    ��   ����{B�Ǎ�mPD�q��#h��            (�/�`0 % ��1/pK ���  <Z���R.��/)4�d,F��4�"
5$P¾+jo���1&�>����ؒ�^��R�h{a�7'�#�	�;�y[��Ia.`�rl�^�t�P{e
�*aeP+u!�0m�v)�W�p�Q��6Z��*�㵥b�+l46�oJu���f�d�GlS[��k���Qa�2�M�����$�HPX�<�-�ۑ �	 �I{Hl=���qg���p�D��    0    p  M    ��   ����[},�%�x�w�7��<���            (�/�`5 �I7P	� �?�����ciYv<��,�$Y�^@|�$UWIn��5�)#�ګB���3�Z: < ; ��Q�s����a
_mNZeM�4Z�7$~��90���R=��NN��$�:��Fc�� ����%#i�b�=La��'f
�-(����/P�tA�i� NQ�E9��T�Kzt��E1���1����`��@hY%��(p�� ע�x�J��u@v�7RZ^	;���U"�%J���n[U��Δ��?�o0~ױv�`s*--�a	�� �J"�)�n6�E��}m�a� jh@�0[o��A\�����Z}qȂ�2B'�WZ*��'�ï ��Z�MH��.�R�
|pD    �     �  D    3x�����������F�mR�;^u��]ֱ            (�/�`  � �.-@m �9�.�b�;��I*eFVNf8�Y����l,��c������z4� �;#y/ht>RnEuaQ��0b��퉈�0��	(f�jE��N�z0[\�sWȶZP�����@H�0��L���ہ� \UY�,�T�"Krmrͪ�`���7�ɸ|3^^i�2�Q��<�O^�[c4���;
 1ӿU.o�z�����^U�0"B�    �        D    3~      n��( &����_ꀙ: X�            