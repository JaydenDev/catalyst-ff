        �  �     �^�����������B��ԥ5�^�\��ـ���            (�/�`�E ��{0`I ����{?��v��I��=%��q�`)%j�h�#��6v&q2C(?�#n t j /�H"�]�Ww����\.�<qfi�*ɱ5�����M����������'^O����j�T�<�0ObqF�T�Y�j�M��<�i����3�fzi �7 g$�țD��M��y����1q���,�BG _ hX|B��)GP�hSK�RHi?��Q
ieR �-{����>%J��48�"�<�3=����t{�����z�ٴҗZ/g���O�{��iB.���]�%
��윐��?��3�1Rڥ�ጻ�V����sa���2X���n�d���*����0OD�(��V��Uo�S���9��)r_��BmK�����-LC�`.y�SZ��:f.���5VY�n�٘C��� �!����Lײ�E�=�R��4��4-��8�	x��#��,]��Uݪ��I�����u��v��a{��!�`@�E�F�!C@	   �0B`,,@R4��0#q3/>�t�ft��W\��⠾�h �|�B�?�N)�-0j_��]�28=�Qu��Ҕ(M˂&tg�Q\����Ob݀�\�]��^T��홍k7��>M�����ᙜ���l�1F"����S�\�����"(��_2HJB�b�    �     S  �     KA    �����t0�P����"H*b�k�%{G            (�/� �U "�G@ [7$O#?��G�K䙑����'����0�2O��؋=����Q�nhBt�:
 �c�1�         P  �    ��   ����in��U�K&rțA(��V                #   D[Constructor,
 Func="mozilla::dom::SpeechRecognition::PrefEnabled"]
    U     8  �    �   ������NZ.��pM$�ԻVs����                I   , Pref="media.webspeech.recognition.enable"]
    �     9      "�   ������e����%���+4(�P�              �     -    void start(optional MediaStream stream);
    �     /      �n   �������t��g!����`��m8C              �  �   #    [Throws, UnsafeInPrerendering]
    �     a  D    ?   ������B��[��M��;˴�<                ?   U Pref="media.webspeech.recognition.enable",
 Func="SpeechRecognition::IsAuthorized"]
    V     8      J   ����G��:H��<��]�,2�                h   , Pref="media.webspeech.recognition.enable"]
    �     a  D    �   �����7d�1��'褽:Cj�# �                ?   U Pref="media.webspeech.recognition.enable",
 Func="SpeechRecognition::IsAuthorized"]
    �       *    ;~   �����0��p@���C֔R�;H&,�x              �  �      �  �                  	 ;�   	�������Ý���~������7              )  6                  
 <�   
�����;�"���著4Š_�;j              O  \             @  !    �V   �������d�\�u�-�_��j�              �      4    [Throws, UnsafeInPrerendering, NeedsCallerType]
    _     *      ��   ����1���2�AuMwk�M|X�              �         [Throws, NeedsCallerType]
    �    k  �    m�   ����$�0ߙϳ���F12���S�            (�/�`b �>*PQ՗e� �HJ � ��ėW������H�m�e��-�3 2 4 ��I�B���O�� G5���tY�OD�����{�iG!DƝ�� �����ښN�ɮ�*��ýP)?�X�%5��u}^�%��v�SW���(ڡ��ù�	�T�+�~��7T�"c�Ȣ�V&4�
�N6T�Q�u���TC����wwL�0	L�X��r>Y�+V�R�^%%7�.�ٰ�`��Ew�}�GԍAC��O�' 0B�(3�(���6^���8��Y��4��IU�b	Tj+�@��(.���p������ d�3j��!�����H���Rx��%��    �     w       m�   ��������)>.���h��l�|            (�/� �u ����
��<6m�˥8�JRa��"�1W@ZI���{���iq����Cܜ�t���i<f-��Ã�uIn���:������%�X_��Hx��9�RJ�� M$^     k    �      m�   ������70�+�M����K�F�            (�/�`�u YO2�9d0���?CL �Ԉ�)a�b%�;y	/H�Q��޳m�v�����<����E ? B 7٧(ww�*4�Q `�P���Nud�斥%��'��Vo����J1����{�b�%��
3�0�$O
H;Ubi���[���X8�S�Y\��G "w�]'!��dF�˗�lx�'���-��ʓEF�k�W�
�@�~��夭��\��	��H03f��E�lt�t�I��W����N�:�+�Em6w��<B��6�J�J���\j1��ߜ�e�م f̾�����~fU%�F����, 0Bc�2��ltjc�o������%�. kte��84��ϒu K\f��@܀ǘ�r�/T��l�f�	a��Z�a[�yb~�B��v.0�f��E℠l[�m    	#     e      j   �����^|��r�����߇1���                ?   ,[Pref="media.webspeech.recognition.enable",
  �  �   !    [Throws]
    constructor();

    	�     F  /    u�   ����6��Y��| �֖\g              1  Z   : Func="SpeechRecognition::IsAuthorized",
 Exposed=Window]
    	�     7  Z    ��   ����;ϸ,5�$��v3ư�n��?v�              1  1   + NamedConstructor=webkitSpeechRecognition,
    
     <  _    	г   �����s��X����JF��T�sכ��              1  \   0 LegacyFactoryFunction=webkitSpeechRecognition,
