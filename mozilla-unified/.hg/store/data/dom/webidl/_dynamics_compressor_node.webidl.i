          �     ����������>�~Pʌ�����Wh̀�#�L            (�/�`� f�h/Pi ���?c��Z��I��=w��y͠7)6�"���Odε���Z Z a K!O|YڜIrlMl��s���P��r�3�Y�%�d�x�7��Y��'Ԣ�3:�y�Y��0덦.b��f�%s�����R���d9�`�PO��	�R�4�"�Rf=�D�>��'�$�4*�Y(�$�&o�H)������N�����i(��ӇZ)gٓ��Y�{�)�H*Ү�Ǜ�f�&�d�	�EU�$�9���<�]F�Zv���d���JkS�5,#^r6����V�����a���C��P8�����DxC}i-j�q��K])��%i+����p�0fcnW:@,��5�|��iu��t��ѕwz��`m!��#|(i|��e������*��0�9����F�*υ��v�(`�ھv�x�I$ C-;<���+Q�}��U�'��X3�Оd?�'X�����pq�t}<�݅<��'ڑ6��pH90'B������D��j�%5Q$8=           �     ��    �����8��9��I���2(�(�#                             Z  �    ;^   ����a�nl�k}��t��ǖ��M�              �  �   N// Mozilla extension
DynamicsCompressorNode implements AudioNodePassThrough;

    r     C  �    �a   �����N4R��?>�����S              �  #   7    readonly attribute float reduction; // in Decibels
    �     9  �    T   ����`�Įu�1%�9���x�              ?  �   - * https://webaudio.github.io/web-audio-api/
    �     *  �    B�   ����}3�Qߨ�(PfX~�B��              �  �   [Pref="dom.webaudio.enabled"]
         �      �L   �����S���/���������            (�/�`_ � �*+�)��<��<��K�;)�˷��R&s�|>H�AȾ�m}C?	b`B�3�[b;r# ��#{Fn��Сq?��c#��n�+Rb(c���A�̕�>XԚ�p����k��q��ak�u�H�n}+����:^�瞄#|J$ϹR�p3�s4N>��u{�AA(  �
� Fp�3,1Vs �\���!XZ(��F������,    �     `      ��   �����%N�_�R���I6�J�              �  E   T Constructor(BaseAudioContext context, optional DynamicsCompressorOptions options)]
    R     \      �=   ����he#|\x�:�:�A th�N              �  I   P Constructor(AudioContext context, optional DynamicsCompressorOptions options)]
    �     `      ��   �����[�z����c��^�J��              �  E   T Constructor(BaseAudioContext context, optional DynamicsCompressorOptions options)]
         w  F   	 m�   	��������u�"�D.���d=            (�/� �u ����
��<6m�˥8�JRa��"�1W@ZI���{���iq����Cܜ�t���i<f-��Ã�uIn���:������%�X_��Hx��9�RJ�� M$^     �     \     
 m�   
����.��62���x6�"�op�	                   y   P/* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
    �     e  "    4   ����RkDx��DT ��`�IH1�mh              �  I   Y Constructor(BaseAudioContext context, optional DynamicsCompressorOptions options = {})]
    F     `      4   �����ǀ���*�:=�jQ!�v�              �  N   T Constructor(BaseAudioContext context, optional DynamicsCompressorOptions options)]
    �     e  "    4!   ������K�p�1�%߉�s�=�<	*              �  I   Y Constructor(BaseAudioContext context, optional DynamicsCompressorOptions options = {})]
         `      4$   ����cFG��l�(G���Ϝ���t�              �  N   T Constructor(BaseAudioContext context, optional DynamicsCompressorOptions options)]
    k     e  "    4a   ������XH��1�(*�5N�����              �  I   Y Constructor(BaseAudioContext context, optional DynamicsCompressorOptions options = {})]
    �     *  @    _7   ������.?]�=�^�V��(��              �  �       [BinaryName="getRelease"]
    �     �  `    j   �����k��Ju���Ğ[�            (�/� �} �#�I ��<ϳˑ���J�w���׮��iC���A����0� gޭ��V��m�I��Ⓝ+�j	�nc�vo|ǆ���=}-;��c�%��Q"� uY)���Bm�0���D�d F��g�9���V6 ]C ��1W    �     B  ^    r�   �����]��w`�d/:��j�`�7D              '  _   6DynamicsCompressorNode includes AudioNodePassThrough;
    �     ;  o    u�   ����Ys��48���mv�s���              �  �   /[Pref="dom.webaudio.enabled",
 Exposed=Window]
