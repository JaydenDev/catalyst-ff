        �  n     �����������"�J���Ӯ��1h�0��            (�/�`n� 6^/`	� ����{?Iӗ>���A�۶τ��u��`@d���(�vH���O M X �?FP�
���L8S|�G�\3�N��q!�+K-%qEk|f��t���1��f�i{�E�
��J'�ǀ���qFa��W���f!��r@+���RH��;���6��d�x��n�>�+yӞ�����=���Db���x�ho�h|��q劭s&�ӹ뚦P�6<6�9�/x���G��@��C��*0<! ���
o(�p"1�E5�����L��dIۋ�$\�m��$Mdm-E�����|�J-$(�x��G	���{�R��+��MW x��,��V5h��~�xB(�&N��Q��qz?�&�������%�/�^�N G��2�z/z���.f�T�����Bƈ��D;6]Å���C����]�ǫ��m	n�HPz    �       ]     ��    ����1�o����F{8����j�z                        �     L  �    ;^   ����^dB���D[`T�}�۱��              ]  ]   @// Mozilla extension
GainNode implements AudioNodePassThrough;

          9  �    T   ����]���F�b���(h��V�Fd              ?  �   - * https://webaudio.github.io/web-audio-api/
    Y     *  �    B�   ����f*�Wz���G�אG�X%X              �  �   [Pref="dom.webaudio.enabled"]
    �     �  0    �B   �����*�=\�1$CNi�kS�y            (�/� �� �G$�)��l��R����$��������-�`rG�/�J��Fk)� TԺ:L����?����#M�iLIۼV�ɂxk�����\�),�B�n��Ԥi��
wM�ЃV�E�Mh�m�{�� 1	 8��a��c52n��q�&�"�N         R  4    ��   �����3�˪%?�U��!s"���              _  �   F Constructor(BaseAudioContext context, optional GainOptions options)]
    o     N  0    �=   �������D������
$�Y�V�              _  �   B Constructor(AudioContext context, optional GainOptions options)]
    �     R  4    ��   ������15�JM�;�<h�̰/��              _  �   F Constructor(BaseAudioContext context, optional GainOptions options)]
         w  ]    m�   ����+Q�
zQ���M���t�            (�/� �u ����
��<6m�˥8�JRa��"�1W@ZI���{���iq����Cܜ�t���i<f-��Ã�uIn���:������%�X_��Hx��9�RJ�� M$^     �     \  4   	 m�   	�����:�ZK����|$������zZ                   y   P/* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
    �     W  9   
 4   
����6JgP�[��B��V8a�              _  �   K Constructor(BaseAudioContext context, optional GainOptions options = {})]
    9     R  4    4   ������������0a^��3E»@�              _  �   F Constructor(BaseAudioContext context, optional GainOptions options)]
    �     W  9    4!   �����eX7'��K;��=W�����              _  �   K Constructor(BaseAudioContext context, optional GainOptions options = {})]
    �     R  4    4$   ����[�w�Q!7leFDI/!��]�%              _  �   F Constructor(BaseAudioContext context, optional GainOptions options)]
    4     �  9     4a   ����퐵��p���(���;
�%            (�/�`I e �/)`ke0��@�#B��G�g�n7WtŊ,8D�D�[�˖�OEe�U�m�c��e���~�º���/���[B�BQH_[W��?�w�	���a��C4��[��˧$a,��T�!h
R�K�7�&�'g@Ї����*������*`Ad,������DY�M5�S�G8��]��(��D�b W[��Qc��x�!T�0�ܱ�3wda
��Ά�C/f�ͻ1k�    *      I     j   �����yq��ּCd���RD���Q            (�/�`e � r0%0��@0��t���iOI���o��s+Y%�VUK1�=V�9�1�#��v�����u�kgP�i�[2�#P�(�ѭ+�O��+p܄ �?e���	%�Ge���I����Z@�����Ҟ��@!}�����h�0��Ј9��}���j��L�%S�d6��e�Fʬ�񦲕X�8���[:�LfF$2� s[���mc��x�!��0�ܬ j���Bx���2Sw6�z1�oލY�    -     4  G    r�   �����Ӵa0�r�e_�~Wv�5��2                H   (GainNode includes AudioNodePassThrough;
    a     ;  X    u�   �����x�g<	�et�)$0ߞ9              A  _   /[Pref="dom.webaudio.enabled",
 Exposed=Window]
