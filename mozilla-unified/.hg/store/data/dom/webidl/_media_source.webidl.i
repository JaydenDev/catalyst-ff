        �  �     ���������	���pP������^PD�@            (�/�`�E fj|0P	� ���?c`$6���Ҭ�>����8E�0P��o�����0K�n t m i9��R�z^j���,�T�(|��3��(:<�k6"�!��ש��^� ���-pe�.�+9l��
���3Z�$����Yo��(F�N?e���8cj�,N���@��6rG�\o���7����ͤ!���	_ XX<�Di|���@jSR9�d(S�qTOeb�<pUl���x��Dq��q�@އ� F�=�����eG���q4�ԟ�YδcԸac�}��.ʧσ );�����[�e��5൴>^���-���h@`L�ӧ�6�p�B�̃kgK.,�!r*>e��*�E���~Jŵ���pRʒ�ͥ�kQ6�0��B}8|�sL�歪(��2��/��F_�����6�
�����V���)�qK"$˧��m��Ȯ.�˖�g@��4a�V��J�߰AX%)��Že��J������J��_cE��2
J0 X� B�ev �9(PLa5a��X����D�C��ڑk)&�S�><f0���ȅik��r��þ�/_�~�0�ng�!�XX��^%=O�8�#DH⩎pm��+�� 9�s8���?"-rx<'ޛy%6j4>=a\d�$�ɿ��9g��B�x"^,4J"�Ȇ�    �     "  �     _�    �����C�
�M~�3� 1i=<���              �  �     [NewObject, Throws]
    �        �    _�   ����3�:x"_u���CW:o 4yw�              �  �     [Creator, Throws]
    �     "  �    `s   �������:�l�3D-�\�*���3(              �  �     [NewObject, Throws]
         E  �    ��   ����.��k��t9���Z��L�Ʉ              �  �   9[Constructor, Func="mozilla::dom::MediaSource::Enabled"]
    [     N  #    ��   �����3� ����#��w� 
��              �  �   B  [ChromeOnly]
  readonly attribute DOMString mozDebugReaderData;
    �     a  �    ?   ����.6X~���?�D	�|��Ȓ�            (�/� z� �  � n  [Throws]
  void setLiveSeekableRange(double start, end);
clear);
 Gx��\�    
     P  	    A   ����+��(l �\��9GU��dr�            (�/� �= �  � x  attribute EventHandler onsourceopen;
endedclosed;
 �"u�e�e    Z     w  2    m�   ����SQ�Y�4ZZf�xMv%z��            (�/� �u ����
��<6m�˥8�JRa��"�1W@ZI���{���iq����Cܜ�t���i<f-��Ã�uIn���:������%�X_��Hx��9�RJ�� M$^     �     \  	    m�   ����5z9A�y�_GTj���1׻H                   y   P/* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
    -     `     	 �   	����hJê�,}�/��#k1�/?��h              �     T  [Throws, ChromeOnly]
  Promise<MediaSourceDecoderDebugInfo> mozDebugReaderData();
    �     N  	   
    
������R�&��BV`�"PҴ�up5              �     B  [ChromeOnly]
  readonly attribute DOMString mozDebugReaderData;
    �     `      )   ����*W@���z��~���^
�              �     T  [Throws, ChromeOnly]
  Promise<MediaSourceDecoderDebugInfo> mozDebugReaderData();
    ;     <      =c   ����(+q���P����)���Z�              �  �   0[Constructor, Pref="media.mediasource.enabled"]
    w     E      >4   �����x����6"GA�˳&ܢ              �  �   9[Constructor, Func="mozilla::dom::MediaSource::Enabled"]
    �     <      >�   ������_�Zڮ�W΋�]�:Ԡ              �  �   0[Constructor, Pref="media.mediasource.enabled"]
    �     X  "    j   ����oK��L�}x'� ���>��              �  �   #[Pref="media.mediasource.enabled"]
  �  �     [Throws]
  constructor();

    P     @  3    u�   �����k�s�fA�͉�P4[l�              �  �   4[Pref="media.mediasource.enabled",
 Exposed=Window]
    �     4  2    	�&   ����L�x; �Bۭ�L~@E:��              A  j   (  attribute EventHandler onsourceclose;
    �     &  5    
�   ����k��7�B������C"2.�S              �  �     [NewObject, ChromeOnly]
