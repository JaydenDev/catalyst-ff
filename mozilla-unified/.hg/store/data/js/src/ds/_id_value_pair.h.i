        �  g     3���������?k�ϸb}#�����+8�Q�            (�/�`g- F�N%�8�����(�>�+�,�,[cdX��00  +�qC B D ���;���7���V�Cm�̙�O+�8�����VRG���
��#5ܫ�\ 0��pF�0͙[5/|Oyv�!�|w�w�;�7������"Kr7M1#q��J�*�g��N"��{��W8���f�y%��`*7*T0ɡZ'Qkr���BW�Xc� G�A$��PM�j��tKow��r]G�ҽ[���6�J}��:Զ��1���i�0]���X�܅Ec���x�
.�3��{�<�fa�gU͌YΓ�sV�I���Ǔ�Zӓ �O�]a�Nr:0�fX-��9��Ҁ3�s*U1��q P��J�zx��C��@�Z�B̃NjO���    �     *  �     A�    ����)Wz�pĮ�F%v���.���              �  �   #include "NamespaceImports.h"
    �       r    A�   ����d�m̿�� ���~�p              t  �        �        �    DP   ����
"p��g6�%��!>6�@              �  �   #include "js/Id.h"

    �     1  �    ��   ����tm!�	ő�m��n�S\y�Rq                    %    explicit IdValuePair(jsid idArg)
        2  �    ӫ   ��������n��H5�!k%f��            (�/�`"E	 "�5,�0�&�@"䕭Hw�{�h3��Mp7)������0(�/oB�e�T���2�V+��DI��d&�M��u҅Z�q����*5F�+Sz/�қ/=ӽ�$�o�!��?�	p�-��M%`�<�:֒ձ�yd��Ep������)��ii���}-8 νM;�����U9��̫Ph�������Q����?uι�s� ��T���Kװ!0��:,�������j��*�*<�ML���h7�i5��Qim�(�9�@1��+a��0Sl՜GE�y    H     -  �    ��   ����9 R�p��f��G��              �  4       IdValuePair() {}
  �  L        u    2  �    �   ����|x]g��b�����6�q%��            (�/�`"E	 "�5,�0�&�@"䕭Hw�{�h3��Mp7)������0(�/oB�e�T���2�V+��DI��d&�M��u҅Z�q����*5F�+Sz/�қ/=ӽ�$�o�!��?�	p�-��M%`�<�:֒ձ�yd��Ep������)��ii���}-8 νM;�����U9��̫Ph�������Q����?uι�s� ��T���Kװ!0��:,�������j��*�*<�ML���h7�i5��Qim�(�9�@1��+a��0Sl՜GE�y    �     C  �    ��   ����=��z�a�&o sm)���u6              @  w   7    explicit AutoIdValueVector(ContextFriendFields* cx
    �     C  �    ��   �����,��_��R�n�-              @  w   7    explicit AutoIdValueVector(ContextFriendFields *cx
    -     C  �   	 �
   	������Mn0��GP#Q�����U�              @  w   7    explicit AutoIdValueVector(ContextFriendFields* cx
    p     ,  �   
 �   
�����fO�	?�e�-�>J��X              t  t   #include "jsapi.h"

  �  �        �     �  �    �M   ����,D�=H��,�!��7x�=�5�            (�/� �% 4  �  G   Wclass MOZ_STACK_CLASS AutoIdValueVector : public JS::RooterBase<Pair>
  �     = : (cx, IDVALVECTOR)
 [��Tnv�
���    )    #  %    =�   ����hH��eX��|������_J�            (�/�`� � R�01`M�[��+Y��WX)x���}l��$��Zk0��w�-��Qa?I�԰p�����lS ۄC���A�$Յ�
��Q���,kb�/��cq3�z]��=.��&ivU���hv���Ud.���}����.Ɣ��IO��������O������uũ���h� ��@uFX3��>%VE��8v$ `3���S�(f �
hń�v��a&ʐN��;x��r��/���$7��� y�����T��y���^q�`�Hd�w�#?�,    L     b      ��   ����d!�E�Z��{{�rQ�T ���            (�/� j� d  � #include "js/GCVector.h"
  �  � �  -using IdValue = <Pair>;
 <��R�z	M�}�    �     =      �   �������_����n�Q��K�e              �  �   1using IdValueVector = JS::GCVector<IdValuePair>;
    �     ?  "    P�   �������0�R�X�1�_���$�              �  �   3    IdValuePair(jsid idArg, const Value& valueArg)
    *    M  %     ��   �����j��G�����@�k�}�֎f            (�/�`s
 ��40�K30ߌ�gNp��Bd�,i��c��/L01F|��3�bӗs������:�?�M	H�U1_�ӭ`	2����H��P����U)#0���,`U/6����Ec#�n��L6C�T�gZ�ߏi1���H((�z�=ϋ�[D�$���7J������h����.7R��v��uN�ڑ���ʌ�$��9�N�Y��P��!��a�}@�f4�. *P�u �0��� Dl�q
l"�i��0��0W�F��ˎ�`曻x�I��N��dW�`�hdp��U��.6�`�\��:�n�]����-����9"    	w    B  �     ��   �����m9��zz],�D諼>�g            (�/�`P�	 ��6/@m �9��?3=ѶB�ro�cG"��,u	�/���T1JГ�y����I	����pk�0���ed˖�E�ȟX���F�/�{��r3�z/�^�f��3�Z��?VUp��`̿e0��C�TQN������bJL�E�I�/�R�&]�7�k���.8��o�� 3Gü�079��d���:#�(Z͹��RKe�Z�@�SfW�:��5�$ �����P'�m�?1?�c�2����<�u��!ie��PX#բ����%6X����L�	w/h���7ף5FM�kc��ȃ���]"    
�     2  �    ��   ����`/���i�'�	�)�9gBt               M   s   & * vim: set ts=8 sts=2 et sw=2 tw=80:
    
�     Y  �    ��   ����/��H�B���E׃?���                   M   M/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
    D     =  �    ��   ������拓8��փC$�8i���              �  �   1using IdValueVector = JS::GCVector<IdValuePair>;
    �     �  �    	h5   �����^�gQn=��X`~.~a            (�/� �% �� )pQ   ��
DH�uftΒv7'"�&�.�S���"-���/y��*ڂ��n��b�L���}J��1aD�$��0���R*� ���V�~�m h��C%��h��vXIl��J;�ʶ0p��cs� L3�
�	�5�U`HM�Y���g�;l-�    .     L  �    	�'   ����S�U�j��=��s�ˎ�6l                S   @  IdValuePair() : value(JS::UndefinedValue()), id(JSID_VOID) {}
    z     Z  �    
\�   ����aY9��Y�2��s��A�'�;                R   N  IdValuePair() : value(JS::UndefinedValue()), id(JS::PropertyKey::Void()) {}
