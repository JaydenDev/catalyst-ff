        F  �     Z[��������{�z�?Y����S����#��            (�/�`� �	 �C%0�� ,-2J\1A��Ƚ�?�ؔ(Ao��gև��8 7 7 �x>ibɣ>_L�epFgt^�����-ѵ^z��qp��P+�k����㌄���/����GD��������7�>���Bn��A�N0���H�~>}��ˉ��>�8�	S��j�l��pP#`
�#a3cJ��n�寶v�m�;,R���V�'�RG�7��8
7��+�jL/��o�#���� 1;�('��(1��*W�� �i�&.k� T@�
�+���,;DЮ�j�Ԃ�qP�.a��UU�7Q$(=    F     G  �     �     ����e�O�{v+����(-O
i���              n  �   ;DefineTestingFunctions(JSContext *cx, JSHandleObject obj);
    �     \  2    ߈   ����/HѼ���;ϯ�Gjy�k              �  �   PJSBool
testingFunc_inParallelSection(JSContext *cx, unsigned argc, jsval *vp);

    �      U    ��   �������@N��+�hX��R�8C             (�/�`L � 6�8# �:+I��8�M�ɴF�@��H\��X �(0 - . N'G�2���wL)��5�P�;w���b����NB9[ؗ`P ���Ⳟ�V�	��	+k	�*8��s����w�y������|�҄/?���y��{��]�H&��Q0�3	O$Ǆ
�:=Kr7M���h�T�����o����i
f� �H;���Zs&���=�>�뷏�Z�'r҈U�K�סl�i� �c��WkTB(�y $eg�꽂6    �     �  2    ��   �����.�R'���Ĺ��%��*N�            (�/�`) � �1#@i� �`H����Z�\Q��;#L4U�B!��hISgM��i4-Ab��8����J�\׏/��	���?��G���s��{�i08��)8S����:��e�����Ը��m����HҾ>}�7T�7��}$1�52�
��>�9�jISWZ����;�[��2Z�^-5��"p�鸏�\A W4r��U8 ��A �1��%Lp����ܖ�&)�h    �    �  U    ��   ����� H����)_:˱dԂN��            (�/�`U &R&�8\SI�Ț�u�X���
��5�LNv�IX  ���G D G ��Ctp������7����N~dQ̛3ƣN��u�r���hqxj�t���GU?�8S�7��mÝ�y��˾&����7�[���e��)�?O���1�F��钼A��@��be?����ee����X��|�x:���O�.��YF��0I�$����@�ڜ��EJ��?�S E$���Aѥ�z�g�Gm=�cs�⋄J�V��j.I�D��z��6W�p��e�(�+'0!lV�D���]�	���@eZ��/��	�׼#��>&��w'�(Ε )��'��WH�i��	�8v�8vMf��U]‪�?�@	�x�p%���Ԟ$X+jU    g     �  2    ��   ����$�B�or�5���S�N��D0            (�/�`) � �1#@i� �`H����Z�\Q��;#L4U�B!��hISgM��i4-Ab��8����J�\׏/��	���?��G���s��{�i08��)8S����:��e�����Ը��m����HҾ>}�7T�7��}$1�52�
��>�9�jISWZ����;�[��2Z�^-5��"p�鸏�\A W4r��U8 ��A �1��%Lp����ܖ�&)�h    P      U    �3   �����3- ��s���i�L	$jP�            (�/�`L � 6�8# �:+I��8�M�ɴF�@��H\��X �(0 - . N'G�2���wL)��5�P�;w���b����NB9[ؗ`P ���Ⳟ�V�	��	+k	�*8��s����w�y������|�҄/?���y��{��]�H&��Q0�3	O$Ǆ
�:=Kr7M���h�T�����o����i
f� �H;���Zs&���=�>�뷏�Z�'r҈U�K�סl�i� �c��WkTB(�y $eg�꽂6    X        i    $   ����X�o�DXx�����}��|@�<              |  |   #include "jsapi.h"

    x     Y  {    �   ����}�^�	��Վ1��Q�]X��?            (�/� �� ����]E�٭�gڢ/ݦy���g)�һ�ؾ�c�^ӥ����Cc#;��%��0y,��[}b��� �
�FA    �     E  y   	 �   	������km��?-p������              �  �   9DefineTestingFunctions(JSContext *cx, HandleObject obj);
         R  �   
 -�   
������M������a�����p              ;  ;   FJSBool
testingFunc_bailout(JSContext *cx, unsigned argc, jsval *vp);

    h     "  �    1[   ����D�z�i��;��,LGk�O              �  �   bool
  ;  B   bool
    �     *  �    A�   ����y`�����}� @�Ep�h{��              �  �   #include "NamespaceImports.h"
    �     y  �    A�   ����PNhjB6b�Ho
}o�c�O�            (�/� �� ��k
& �I�AқdS��+Cj��dUA�I~��*6
�%�_�@< �e[t�Q��s$/����:�ffhf�*��]|P��P����l�'����G�? 4a�w6-�    	-     V      G�   ������8�y�?X�Cl�~���S              �  �   Jbool
testingFunc_assertFloat32(JSContext *cx, unsigned argc, Value *vp);

    	�     W  "    [   �����م�[����~�9D��`sR              �  �   KDefineTestingFunctions(JSContext *cx, HandleObject obj, bool fuzzingSafe);
    	�     �      ['   ����
���������q$,����             (�/�`� e �N1+pi���%�{l��;Rz��g��	L�~DBp@ɥr�^���������}��C�C�_ �-BIW*e��H�?@�WD���t���n�{��J�:�
a��/���iT���i%RP��0��=�/�	&�����?��kA	�]E�r��QR--a��M�x����>gQ �q��L�-��m�3b�>�����?�� �6dh�|�y)L��-�Nh��~�l+5�
W1è=    
�      "    _   �����ٙ�#-�]:�����<���            (�/�`� � r�3+`������]�_R��E�e��OOR��
:0f��Z��?�[�Jn�
)Qǭ�i�oX���/����6�%'|���� ��B��<@��-}0iZڶ%�=����5���TJI�Ej�l��a��KU��D
�Z/}h�R��!�Q0���%�_1,QN0�9X�2q1+����c͏�f��Q�JlȒ��H�2.C�Y>���m�����?�� i7dh�|�y)L��Q��h���Yl7�����F�    �       �    ��   �����za_�dt��Tnd�p;                [        �     `  X    �"   ����|4�l@�v�q�����q�.�            (�/� �� �  � �bool
testingFunc_inJit(JSContext *cx, unsigned argc, Value *vp);

Ion �9D�ypY    ?     a  �    �r   ����QC��r'�<�j达��X�d              �  �   UtestingFunc_assertRecoveredOnBailout(JSContext *cx, unsigned argc, Value *vp);

bool
    �     �  �    ��   �����c�� ��8.���B��            (�/�`� u R+,�9k�s��6(/e��~���'%����(��.�,`�C�2X7Pj~����F#���HX�)+����o�~
GA/n��ҹ��?���
6�����P��n_?n���F�/Q���2Zi�ry6����,)��ڵ���a�-��
�Ȁ[�f� �j�|��(�� �`� �9���vc�N\�L�O���M�
IjQ    x     �  �    ��   ������jqk���:e8(a�E��            (�/�`� u R+,�9k�s��6(/e��~���'%����(��.�,`�C�2X7Pj~����F#���HX�)+����o�~
GA/n��ҹ��?���J�����P��n_?n���F�/Q���2Zi�ry6����,)��ڵ���5�-��
�Ȁ[�f� �j�|��(�� �`� �9���vc�N\�L�O���M�
IjQ    P     �  �    �
   ����;��Un���Ә`8y����            (�/�`� u R+,�9k�s��6(/e��~���'%����(��.�,`�C�2X7Pj~����F#���HX�)+����o�~
GA/n��ҹ��?���
6�����P��n_?n���F�/Q���2Zi�ry6����,)��ڵ���a�-��
�Ȁ[�f� �j�|��(�� �`� �9���vc�N\�L�O���M�
IjQ    (     �  �    �   ����uTӈ����娯"�����7            (�/� �� �I"&p����U="D�v����IDZHB	#������4a�8z���F���Vʥ�WP?B�";�2Ф�h�����3·�A?�3�O��X��o��0F�	Ģv[�,txh"5EZ���($�׍����܂�����?��� L���M�
IjQ    �     �  "    �O   ������J4���#E*��uvEX�            (�/�`E � �J&)�Âa1�,�s�n��^&�|��>��T,??��~�q�X��m�~Թ\�g���_[�)��%(*0ѷ��ZO�Cy1�O���|ҁ�D7�C� ��d��E"��m-��H[�#5�:8�@&���,8n�?f1k���х� �1 � L�����`��+�	-1���    �     �  �    XE   ����╇L��7�M��91���            (�/�` 5 R�$(��	��D{�'�Ν|/�K��oP��hHj!e`ْ頁������J��F̴A�_����(�oA�����mn,0��/iH!\��P����YK[7M������0���a}�.�<$�FK��	�� \v�6&m]Cu�u�"��O�� ���K��.����    :     q  �    ke   ����V����-���f�ɯ���              �     eDefineTestingFunctions(JSContext* cx, HandleObject obj, bool fuzzingSafe, bool disableOOMFunctions);
    �     >  &    ��   �����,Od�_���mU!2�Z]            (�/� Z� �  �  �   MOZ_MUST_USE bool
  "  'l  q @��z    �     v  �    �6   ������ʬ��I�46*��/Q            (�/� zm b� �� �    Q۽���	?�� ���{�
O2�!��P_k{��#���s��Ȧ��0����;�J�蓱,�1]��r��L���R*�05��`L �<5ʃ�    _    %  �    ��   ����@��	��=�jR*�7Y���            (�/�`�� ��5.pk��_�U]T�!#$��e��J�e�m��h�Ǣ���}J�������zo�h�s��Y�BJ�FL*"�ӕ�m-�r�ά��J���>��6.�u�����F���=�j�"Si�r�g�A3�"WJf���O���1��}�Nډ-���Ĝ�y\"���A�$f��4}:��PaI$C׍g�b�r)�n�}Fʰ*ʪL,���@�_ � !~���`D$�������k��F`j$ �-pL��h:f~[C*���!S������k	�    �     2  �    ��   ����$ə!��j��Mh�fQ?=R`               M   s   & * vim: set ts=8 sts=2 et sw=2 tw=80:
    �     Y  �     ��    ��������ͯ�
U�;8i6~���                   M   M/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
         m  T   ! �   !�����P�c�u{�T��Y��_� �@            (�/� �% �  � �MOZ_MUST_USE bool testingFunc_serialize(JSContext* cx,
 unsigned argcValue* vp);

 ����8�    |       �   " �!   "����fOt� JM��L������Rƍ              �  w        �     m  T   # �"   #�����,�V������3\L��	+f            (�/� �% �  � �MOZ_MUST_USE bool testingFunc_serialize(JSContext* cx,
 unsigned argcValue* vp);

 ����8�    �     R  ,   $ �   $����u��%��O��ר͉��X�              �  B   FMOZ_MUST_USE bool testingFunc_serialize(JSContext* cx, unsigned argc,
    G     7  W   % 	*c   %�����+L)pu�$2����]C5#��              �  �   +MOZ_MUST_USE bool InitTestingFunctions();

    ~      b   & 	�   &������7r&G�=���x��"@�            (�/�`- �0/pk���)P��?O{
��?���J���W��j��4�5e�N=[qS~+�SD��H�Ɗ�۷��a���<��,�4"3��4��İ����U�[U"Z�x~�m�2[I-SsUQRJ(��e\���m�u!)������.�Ұ��*h�����/9 ��8��*Ss���9%Þ.����"���3�? ��;��t����	@��<���Į����א:��B��.���5<d� 8�q5נe    �     f  �   ' 	��   '����fں)��CA�#(h td���              �  �   Z#include "NamespaceImports.h"  // JSContext, JSFunction, HandleObject, HandleValue, Value
